const cat = {
    name: "Paki",
    breed: "kitten"
}

let catAgeValidation = {
    set(obj, prop, val) {
        if (prop === "age") {
            if (!Number.isInteger(val)) {
                throw new Error("Please set number as your value.")
            }
            if (val > 30) {
                throw new Error("Cats can live up to 30 years.")
            }
            obj[prop] = val;
            console.log("Your cat is now " + val + " years old.");

        }
    },
    get(obj, prop) {
        return (!prop) ? "No age specified for this kitty!" : (prop === "age") ?
            "Your cat is " + obj[prop] + " years old." : "Ask for age only!"
    }
}


let catNameValidation = {
    get(obj, prop) {
        return (!prop) ? "Not specified for this kitty!" : (prop === "name") ?
            "This little kitty is called " + obj[prop] + "." : "Not asking cat's name???"
    }
}

let catPro = new Proxy(cat, catAgeValidation);

// console.log(catPro.age = 22);
// // console.log(catPro.age = 120);
// // console.log(catPro.age = "thirty");
// console.log(catPro.age);
// console.log(catPro.name);


catPro = new Proxy(cat, catNameValidation);

console.log(catPro.name);