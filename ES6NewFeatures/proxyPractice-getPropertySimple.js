const musician={
    name:"Toshiya",
    instrument:"bass",
    band:"Dir en Grey"
}

let nameValidation={
    get(obj, prop){
        if(prop==="name"){
            return "fullname: Toshimasa Hara" 
        }
    }
}

const musicalProxy=new Proxy(musician, nameValidation)
console.log(musicalProxy.name);

