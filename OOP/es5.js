// parent class (constructor function in javascript)
function Animal(family, genus, species) {
    this.family = family;
    this.genus = genus;
    this.species = species;
};

Animal.prototype.sayHello = function () {
    return "Hello, I'm " + this.species;
};

// child class (that will inherit from parent class)
function Felinae(genus, species, subspecies) {
    Animal.call(this, genus, species);
    this.subspecies = subspecies;
};

// Felinae prototype object is now object that inherited
// properties and methods from Animal.prototype object
Felinae.prototype = Object.create(Animal.prototype);

// reset constructor in prototype object;
Felinae.prototype.constructor = Felinae;


// Felinae got method sayHello (from parent's prototype object),
// which we will OVERRIDE here;
Felinae.prototype.sayHello = function () {
    var greeting = Object.getPrototypeOf(Animal).sayHello.call(this);
    return greeting + " and I'm " + this.subspecies;
};

// we can define even new method on prototype object,
// and our instances will inherit it, along with sayHello method;

Felinae.prototype.sayMeow = function () {
    return "Meow!";
};

// So, everything written in prototype object will get to our instances.
// there, __proto__ will point to that ancestor prototype 
// from where we inherited features;

console.log(Felinae.prototype); // in browser will log:

// object type of Animal 
// (that is object that inherited everything from Animal prototype)
// just constructor if that of child class;
// and has custom method that only child class has (sayMeow())

// Object __proto__ points to prototype object of Animal:

console.log(Felinae.prototype.__proto__) // {sayHello: ƒ, constructor: ƒ}

// that object also has own __proto__ property, that is object,
// inherited from generic Object's prototype object;
