function substringgg(str, begin, end) {
    var newStr = "";
    if (!end || end > str.length) {
        end = str.length;
    }
    if (begin < 0) {
        begin = 0;
    }
    if (end < 0) {
        end = 0;
    }
    if (begin > str.length) {
        begin = str.length;
    }
    if (begin > end) {
        var temp = begin;
        begin = end;
        end = temp;
    }
    if (begin === end) {
        return []
    }
    for (var i = begin; i < end; i++) {
        newStr += str[i]
    }
    return newStr;
}

console.log(substringgg("Stabilo", -2, 5));
console.log("Stabilo".substring(-2, 5));