function rearrange(arr) {
    var l = arr.length;
    if (l % 2 === 0) {
        var end = l / 2;
    } else {
        var end = Math.floor(l / 2);
    }

    for (var i = 0; i < end; i++) {
        var temp = arr[i];
        arr[i] = arr[(l - 1) - i];
        arr[(l - 1) - i] = temp;

    }
    return arr;
}

console.log(rearrange(["laki", "paki", 3, 4, 6, 9, 0]));