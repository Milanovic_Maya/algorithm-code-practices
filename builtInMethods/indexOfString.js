function sliceString(str, begin, end) {
    var newStr = "";
    for (var i = begin; i < end; i++) {
        newStr += str[i];
    }
    return newStr;
}

function indexOff(str, value, from) {
    var l = str.length;
    var v = value.length;
    if (!from) {
        from = 0
    }
    for (var i = from; i < l; i++) {
        if (str[i] === value[0]) {
            var str2 = sliceString(str, i, (i + v));
            if (str2 === value) {
                return i;
            }
        }
    }
    return -1;
}

console.log(indexOff("Pavilion", "vil", 1));