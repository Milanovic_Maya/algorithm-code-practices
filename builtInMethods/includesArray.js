function isIncludedInArray(arr, search) {
    for (var i = 0; i < arr.length; i++) {
        if (arr[i] === search) {
            return true;
        }
    }
    return false;
}

console.log(isIncludedInArray(["laki", "paki", "cat", 12, 24, 36, 48, "cat", "kitten"], "cat"));