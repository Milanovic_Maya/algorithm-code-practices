function compare(a, b) {
    if (a < b) {
        return -1
    };
    if (a > b) {
        return 1
    };
    if (a === b) return 0;
}

function organize(arr, func) {
    for (var i = 0; i < arr.length; i++) {
        if (func(arr[i], arr[i + 1]) > 0) {
            var temp = arr[i];
            arr[i] = arr[i + 1];
            arr[i + 1] = temp;
        }
    }
    return arr;
}


console.log(organize([1, "m", 3, 2, "laki", 6], compare));