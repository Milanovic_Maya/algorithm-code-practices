function popp(arr) {
    var l = arr.length;
    var elem = arr[l - 1];
    delete arr[l - 1];
    arr.length = l - 1;
    return elem;
}

console.log(popp([1, 3, 5, 7]));
console.log([1, 3, 5, 7].pop());