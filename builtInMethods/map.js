function mapp(arr, func) {
    var newArr = [];
    for (var i = 0; i < arr.length; i++) {
        newArr[i] = func(arr[i]);
    }
    return newArr;
}

function incr(n) {
    return n+1;
}

console.log(mapp([1, 2, 3], incr));