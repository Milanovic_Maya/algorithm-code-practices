function lstStr(str, value, from) {
    var l = str.length;
    var v = value.length;
    for (var i = from; i >= 0; i--) {
        if (str[i] === value[0]) {
            var str2 = sliceString(str, i, i - v);
            if (str2 === value) {
                return i;
            }
        }
    }
    return -1;
}


function sliceString(str, begin, end) {
    var newStr = "";
    for (var i = begin; i > end; i--) {
        newStr += str[i];
    }
    return newStr;
}

console.log(lstStr("canal", "a", 4));