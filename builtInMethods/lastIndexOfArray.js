function lstArr(arr, value, from) {
    var l = arr.length;
    if (!from) {
        from = l - 1
    }
    if (from < 0) {
        from = l + from
    }
    for (var i = from; i >= 0; i--) {
        if (arr[i] === value) {
            return i;
        }
    }
    return -1;
}

console.log(lstArr([2, 5, 9, 2], 2, -1));