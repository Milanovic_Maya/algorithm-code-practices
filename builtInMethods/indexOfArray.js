function indexOfArray(arr, value, from) {
    var l = arr.length;
    if (!from) {
        from = 0
    };
    if (from > arr.length) {
        return -1
    };
    for (var i = from; i < l; i++) {
        if (arr[i] === value) {
            return i;
        }
    }
    return -1;
}

console.log(indexOfArray(["lion", "cat", "Laki", "fox", "meow", "fox", "Paki"], "fox", 4));