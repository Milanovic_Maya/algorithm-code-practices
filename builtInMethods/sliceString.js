function slicee(str, begin, end) {
    var newStr = "";

    if (begin) {
        if (begin < 0) {
            begin = str.length + begin;
        }
    }
    if (!begin) {
        begin = 0;
    }
    if (!end) {
        end = str.length;
    }
    if (end) {
        if (end < 0) {
            end = str.length + end;
        }
    }
    for (var i = begin; i < end; i++) {
        newStr += str[i];
    }
    if (!begin && !end) {
        newStr = str;
    }
    return newStr;
}


console.log(slicee("LakiiPaki", 1,2));
console.log("LakiiPaki".slice(1,2));