function isIncludedString(str, search) {
    var l = str.length;
    var s = search.length;
    for (var i = 0; i < l; i++) {
        if (str[i] === search[0]) {
            var str2 = sliceString(str, i, i + s);
            if (str2 === search) {
                return true;
            }
        }
    }
    return false;
}

function sliceString(str, begin, end) {
    var newStr = "";
    for (var i = begin; i < end; i++) {
        newStr += str[i];
    }
    return newStr;
}


console.log(isIncludedString("Pavilion", "lion"));