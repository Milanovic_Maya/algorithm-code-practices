function filterr(arr, func) {
    var newArr = [];
    for (var i = 0; i < arr.length; i++) {
        if (func(arr[i])) {
            newArr.push(arr[i]);
        }
    }
    return newArr;
}

function grr(elem) {
    if (elem % 2 === 0) {
        return true;
    }
}

console.log(filterr([1, 2, 3, 4, 5, 6], grr));