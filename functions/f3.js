// 1. Write a program to insert a string within a string at a particular position (default is 1,
// beginning of a string).
// "My random string", "JS " ->​ "JS My random string"
// "My random string", "JS ", 10 ->​ "My random JS string"

// 2. Write a program to join all elements of the array into a string skipping elements that are
// undefined, null, NaN or​ Infinity.
// [NaN, 0, 15, false, -22, '', undefined, 47, null]



// 3. Write a program to filter out ​falsy​ values from the array​.
// [NaN, 0, 15, false, -22, '', undefined, 47, null] ->​ [15, -22, 47]



// 4. Write a function that reverses a number. The result must be a number.
// 12345 ->​ 54321 // Output must be a number

const reverseNum = num => {
    let result = 0;
    const digits = [];

    while (num >= 1) {
        let temp = num % 10;
        num = (num - temp) / 10;
        digits.push(temp);
    }
    let j = 0;
    for (let i = digits.length - 1; i >= 0; i--) {
        result += digits[j++] * Math.pow(10, i);
    }
    return result;
};

// console.log(reverseNum(123));


// 5. Write a function to get the last element of an array. Passing a parameter 'n' will return the
// last 'n' elements of the array.
// [7, 9, 0, -2] ->​ -2
// [7, 9, 0, -2], 2 ->​ [0, -2]

// 6. Write a function to create a specified number of elements with pre-filled numeric value
// array.
// 6, 0 ->​ [0, 0, 0, 0, 0, 0]
// 2, "none" ->​ ["none", "none"]
// 2 ->​ [null, null]


// 7. Write a function that says whether a number is perfect.
// 28 ->​ 28 is a perfect number.
// Note: According to Wikipedia: In number theory, a perfect number is a positive integer that is equal to the sum
// of its proper positive divisors, that is, the sum of its positive divisors excluding the number itself (also known
// as its aliquot sum). Equivalently, a perfect number is a number that is half the sum of all of its positive divisors
// (including itself).
// E.g.: The first perfect number is 6, because 1, 2 and 3 are its proper positive divisors, and 1 + 2 + 3 = 6.
// Equivalently, the number 6 is equal to half the sum of all its positive divisors: (1 + 2 + 3 + 6) / 2 = 6. The next
// perfect number is 28 = 1 + 2 + 4 + 7 + 14. This is followed by the perfect numbers 496 and 8128.

const isNumPahfect = num => {
    let result = false;

    const divisors = recNum(num);

    const sumOfDivisors = divisors.slice(1).reduce((acc, val) => acc + val);

    if (divisors[0] === sumOfDivisors) {
        result = true;
    }
    return result;
};

const recNum = (num, arr = []) => {
    if (num === 1) {
        arr.push(num);
        return arr;
    }
    arr.push(num);

    // const newNum = Math.ceil(num / 2);
    const newNum = (num % 2) + Math.floor(num / 2);

    return recNum(newNum, arr);

};

// console.log(isNumPahfect(6));

// 8. Write a function to find a word within a string.
// 'The quick brown fox', 'fox' ->​ "'fox' was found 1 times"
// 'aa, bb, cc, dd, aa', 'aa' ->​ "'aa' was found 2 times"

const findWordInStr = (str, word) => {
    let counter = 0;
    for (let i = 0; i < str.length; i++) {
        if (str[i] === word[0]) {
            // slice piece of string
            const strToCheck = str.slice(i, i + word.length);
            if (areStringsEqual(strToCheck, word)) {
                counter++;
            }
        }
    }
    return counter;
};

// check if two strings of same length are equal 
const areStringsEqual = (s1, s2) => {
    if (typeof s1 === "string" && typeof s2 === "string") {
        if (s1.length === s2.length) {
            if (s1 === s2) {
                return true;
            }
        }
    }
    return false;
};


// console.log(findWordInStr('The quick brown fox is playing with another baby fox', 'fox'));


// 9. Write a function to hide email address.
// "myemailaddress@bgit.rs" ->​ "myemail...@bgit.rs"


// 10.Write a program to find the most frequent item of an array.
// [3, 'a', 'a', 'a', 2, 3, 'a', 3, 'a', 2, 4, 9, 3]

const findMostOcc = arr => {
    const newArr = [];
    const occ = [];
    let i, j, k = 0;

    for (i = 0; i < arr.length; i++) {
        let exist = false;
        for (j = 0; j < newArr.length; j++) {
            if (arr[i] === newArr[j]) {
                exist = true;
                occ[j]++;
            }
        }
        if (!exist) {
            newArr[j] = arr[i];
            occ[k++] = 1;
        }
    }
    // find index of max element in occ array;
    // that is the index of most freq element in newArr

    const occMaxIndex = findMax(occ);
    return newArr[occMaxIndex];

};

const findMax = arr => {
    let max = 0;
    for (let i = 1; i < arr.length; i++) {
        if (arr[max] < arr[i]) {
            max = i;
        }
    }
    return max;
}

console.log(findMostOcc([3, 'a', 'a', 'a', 2, 3, 'a', 3, 'a', 2, 4, 9, 3]));