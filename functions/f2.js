// 1. Write a function to check whether the `input` is a string or not.
// "My random string" -> true
// 12 -> false

const isInputString = input => {
    let result;
    typeof input === "string" ? result = true : result = false;
    return result;
};

// console.log(isInputString("Laki_Paki"));
// console.log(isInputString(null));

////////////////////////////////////////////////////////////////////////////////////////

// 2. Write a function to check whether a string is blank or not.
// "My random string" -> false
// " " -> true
// 12 -> false
// false -> false


const isStringBlank = str => {
    let result;
    str === "" ? result = true : result = false;
    return result;
};

// console.log(isStringBlank(""));
// console.log(isStringBlank("pakilaki"));
// console.log(isStringBlank(true));

////////////////////////////////////////////////////////////////////////////////////////


// 3. Write a function that concatenates a given string n times (default is 1).
// "Ha" -> "Ha"
// "Ha", 3 -> "HaHaHa"

const concatStr = (str, n) => {
    let result = str;
    if (typeof n === "number" && n) {
        for (let i = 1; i < n; i++) {
            result += str;
        }
        return result;
    }
};

// console.log(concatStr("Ha", 3));

///////////////////////////////////////////////////////////////////////////////////////

// 4. Write a function to count the number of letter occurrences in a string.
// "My random string", "n" -> 2;

const countLetterOccInStr = (str, letter) => {
    let result = 0;
    for (let i = 0; i < str.length; i++) {
        if (str[i] === letter) {
            result = result + 1;
        }
    }
    return result;
};

// console.log(countLetterOccInStr("My random string", "n"));

///////////////////////////////////////////////////////////////////////////////////////


// 5. Write a function to find the position of the ​first​ occurrence of a character in a string. The
// result should be in human numeration form. If there are no occurrences of the character,
// the function should return -1.

const indexOF = (str, char) => {
    let result = -1;
    for (let i = 0; i < str.length; i++) {
        if (str[i] === char) {
            result = i;
            return result;
        }
    }
    return result;
};

// console.log(indexOF("Maya", "a"));

////////////////////////////////////////////////////////////////////////////////////////////////

// 6. Write a function to find the position of the ​last​ occurrence of a character in a string. The
// result should be in human numeration form. If there are no occurrences of the character,
// function should return -1.

const lastIndexOF = (str, char) => {
    let result = -1;
    for (let i = str.length - 1; i >= 0; i--) {
        if (str[i] === char) {
            result = i;
            return result;
        }
    }
    return result;
};

// console.log(lastIndexOF("Cats","a"));

//////////////////////////////////////////////////////////////////////////////////////////////

// 7. Write a function to convert string into an array. Space in a string should be represented as
// “null” in new array.
// "My random string" -> ["M", "y", null, "r", "a"]
// "Random" -> ["R", "a", "n", "d", "o", "m"]


const str2Arr = str => {
    const result = [];
    let j = 0;
    for (let i = 0; i < str.length; i++) {
        if (str[i] === " ") {
            result[j++] = null;
            break;
        }
        result[j++] = str[i]
    }
    return result;
};

// console.log(str2Arr("Laki "));

////////////////////////////////////////////////////////////////////////////////////////////

// 8. Write a function that accepts a number as a parameter and checks if the number is prime or
// not.
// Note:​ A prime number (or a prime) is a natural number greater than 1 that has no positive
// divisors other than 1 and itself.


const isNumberPrime = num => {
    let result = false;
    if (num > 1) {
        if (num === 2) {
            result = true;
            return result;
        }
        for (let i = 2; i < num; i++) {
            if (num % i === 0) {
                return result;
            }
        }
        result = true;
    }
    return result;
};

// console.log(isNumberPrime(5));

//////////////////////////////////////////////////////////////////////////////////////////////////

// 9. Write a function that replaces spaces in a string with provided separator. If separator is not
// provided, use “-” (dash) as the default separator.
//  "My random string", "_" -> "My_random_string"
//  "My random string", "+" -> "My+random+string"
//  "My random string" -> "My-random-string"

const repSpace = (str, sep) => {
    if (!sep) {
        sep = "-"
    }
    let result = "";
    for (let i = 0; i < str.length; i++) {
        if (str[i] === " ") {
            result += sep;
        } else {
            result += str[i]
        }
    }
    return result;
};

// console.log(repSpace("Laki i Paki", "~"));

//////////////////////////////////////////////////////////////////////////////////////////////////////


// 10. Write a function to get the first n characters and add “...” at the end of newly created string.

const hideChars = (str, n) => {
    let result = "";
    for (let i = 0; i < n; i++) {
        result += str[i]
    }
    return `${result}...`;
};

// console.log(hideChars("Cats are awesome", 9));

///////////////////////////////////////////////////////////////////////////////////////////////

// 11. Write a function that converts an array of strings into an array of numbers. Filter out all
// non-numeric values.
// ["1", "21", undefined, "42", "1e+3", Infinity] -> [1, 21, 42, 1000]

const arrStr2ArrNums = arr => {
    const result = [];
    let j = 0;
    for (let i = 0; i < arr.length; i++) {
        if (arr[i] && isFinite(arr[i])) {
            result[j++] = parseFloat(arr[i])
        }
    }
    return result;
};

// console.log(arrStr2ArrNums(["1", "21", undefined, "42", "1e+3", Infinity]));

///////////////////////////////////////////////////////////////////////////////////////////////


// 12. Write a function to calculate how many years there are left until retirement based on the
// year of birth. Retirement for men is at age of 65 and for women at age of 60. If someone is
// already retired, a proper message should be displayed.


const toRetirement = (yob, gender) => {
    let retirement = getRetirementYears(gender);
    const currYear = new Date().getFullYear();
    const age = currYear - yob;

    if (gender === "female" && age >= 60) {
        return "retired"
    } else if (gender === "male" && age >= 65) {
        return "retired"
    } else {
        return `${retirement-age} years 'till retirement!`
    }
};

const getRetirementYears = gender => {
    let retirement;
    if (gender === "female") {
        retirement = 60;
    };
    if (gender === "male") {
        retirement = 65;
    };
    return retirement;
};

// console.log(toRetirement(1989, "female"));

////////////////////////////////////////////////////////////////////////////////////////////////


// 13.Write a function to humanize a number (formats a number to a human-readable string) with
// the correct suffix such as 1st, 2nd, 3rd or 4th.
// 1 -> 1st
// 11 -> 11th
// Hint: num % 100 >= 11 && num % 100 <= 13

