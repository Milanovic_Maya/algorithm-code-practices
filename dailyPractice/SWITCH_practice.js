function gradeCalculator(grade) {
    //your code goes here...
    let result = "";
    let num = grade;
    switch (true) {
        case num >= 90:
            result = "A"
            break;
        case num >= 80 && num < 90:
            result = "B"
            break;
        case num >= 70 && num < 80:
            result = "C"
            break;
        case num >= 60 && num < 70:
            result = "D"
            break;
        default:
            result = "F"
            break;
    }
    return result;
}

//Uncomment the lines below to test your code

console.log(gradeCalculator(92)); //=> "A"
console.log(gradeCalculator(84)); //=> "B"
console.log(gradeCalculator(70)); //=> "C"
console.log(gradeCalculator(61)); //=> "D"
console.log(gradeCalculator(43)); //=> "F"



function disemvowel(string) {
    let output = "";
    for (let i = 0; i < string.length; i++) {
        let item = string[i].toLowerCase();
        switch (item) {
            case "a":
            case "e":
            case "i":
            case "o":
            case "u":
                output += "";
                break;
            default:
                output += string[i];
                break;
        }
    }
    return output;
}

//Comment in the code below to test your function:

console.log(disemvowel('CodeSmith')); // => 'CdSmth'
console.log(disemvowel('BANANA')); // => 'BNN'
console.log(disemvowel('hello world')); // => 'hll wrld'