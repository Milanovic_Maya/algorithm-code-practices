// 1What will the following code output?
// for (var i = 0; i < 3; i++) {
//   setTimeout(function () {
//     alert(i);
//   }, 1000 + i);
// }
// 2Write a function that would allow you to do this.
// var addSix = createBase(6);
// addSix(10); // returns 16
// addSix(21); // returns 27

function currying(n) {
  return function (x) {
    return n + x;
  }
}

var addSix = currying(6);
console.log(addSix(21));


// 3How would you use a closure to create a private counter?

function privateCounter() {
  var counter = 0;
  return function () {
    counter++;
    return counter;
  }
}

var result = privateCounter();
console.log(result());
var result1=result();
console.log(result1);
