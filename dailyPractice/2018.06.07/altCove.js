// Program to List all Substrings in a given String
function listAllSubstrings(str) {
    var result = [];
    var k = 0;
    for (var i = 0; i < str.length; i++) {
        var element = "";
        for (var j = i; j < str.length; j++) {
            element += str[j];
            result[k++] = element;
        }
    }
    return result;
}

console.log(listAllSubstrings("Laki"));


// • Get the longest substring with same character
function getLongestSubstringWithSameChar(str) {
    var substringList = [];

    for (var i = 0; i < str.length; i++) {
        var element = str[i];
        for (var j = i + 1; j < str.length; j++) {
            if (str[i] === str[j]) {
                element += str[j];
            }
        }
        substringList.push(element);
    }

    var result = substringList[0];

    for (var k = 1; k < substringList.length; k++) {
        if (result.length < substringList[k].length) {
            result = substringList[k];
        }
    }

    return result + " is " + result.length + " characters long.";
}

console.log(getLongestSubstringWithSameChar("kyuuubbi"));

// • Print words ending with letter e.g. B

function printWithEndingLetter(arr, letter) {
    var result = [];
    for (var i = 0; i < arr.length; i++) {
        var element = arr[i];
        var lastIndex = element.length - 1;
        if (element[lastIndex] === letter) {
            result.push(arr[i]);
        }
    }
    return result;
}

console.log(printWithEndingLetter(["Laki", "Paki", "Cula"], "i"));


// • Reverse the words in the string

function reverseString(str) {
    var result = "";
    for (var i = str.length - 1; i >= 0; i--) {
        result += str[i];
    }
    return result;
}

function recursionReverseString(str) {
    if (str === "") {
        return str;
    }
    return recursionReverseString(str.slice(1)) + str.charAt(0);
}
console.log(recursionReverseString("LakiiPaki"));


console.log(reverseString("Maya"));