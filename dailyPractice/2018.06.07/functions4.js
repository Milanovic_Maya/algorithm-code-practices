// 10. Write a program that inserts a given element e on the given position p in the array a. If
// the value of the position is greater than the array length, print the error message.
// Input: e = 78, p = 3, a = [2, -2, 33, 12, 5, 8]
// Output: [2, -2, 33, 78, 12, 5, 8]

function insertElement(elem, position, arr) {
    var output = [];
    var j = 0;
    if (position > arr.length) {
        return "error";
    }

    for (var i = 0; i < arr.length; i++) {
        if (i === position) {
            output[position] = elem;
            j++;
        }
        output[j++] = arr[i];
    }
    return output;
}

console.log(insertElement(3, 2, [1, 2, 4, 5]));

// 9. Write a program that deletes a given element e from the array a.
// Input: e = 2, a = [4, 6, 2, 8, 2, 2]
// Output array: [4, 6, 8]

function deleteElemFromArr(arr, elem) {
    var output = [];
    var j = 0;

    for (var i = 0; i < arr.length; i++) {
        if (arr[i] !== elem) {
            output[j++] = arr[i];
        }
    }
    return output;
}

console.log(deleteElemFromArr([4, 6, 2, 8, 2, 2], 2));

// 8. Write a program that concatenates two arrays.
// Input arrays: [4, 5, 6, 2], [3, 8, 11, 9]
// Output array: [4, 5, 6, 2, 3, 8, 11, 9]

function conCat(arr1, arr2) {
    var output = arr1;
    var j = 0;
    var l = arr1.length + arr2.length;
    for (var i = arr1.length; i < l; i++) {
        output[i] = arr2[j++];
    }
    return output;
}

console.log(conCat([1, 2, 3], [4, 5, 6]));

// 7. Write a program that intertwines two arrays. You can assume the arrays are of the same
// length.
// Input arrays: [4, 5, 6, 2], [3, 8, 11, 9]
// Output array: [4, 3, 5, 8, 6, 11, 2, 9]

function intertwineArrays(arr1, arr2) {
    var output = [];
    var l = arr1.length = arr2.length;

    for (var i = 0; i < l; i++) {
        output[i * 2] = arr1[i];
        output[i * 2 + 1] = arr2[i];
    }
    return output;
}
console.log(intertwineArrays([1, 2, 3], ["A", "B", "C"]));

// 6. Write a program that checks if a given array is symmetric. An array is symmetric if it can
// be read the same way both from the left and the right hand side.
// Input array: [2, 4, -2, 7, -2, 4, 2]
// Output: The array is symmetric.
// Input array: [3, 4, 12, 8]
// Output: The array isn’t symmetric.
function isArraySymmetric(arr) {
    if (arr.length !== 0) {
        if (arr.length % 2 !== 0) {
            var l = (arr.length - (arr.length % 2)) / 2
        } else {
            var l = arr.length / 2;
        }
    } else {
        return "Please enter valid array."
    }

    for (var i = 0; i < l; i++) {
        if (arr[i] !== arr[arr.length - i - 1]) {
            return "Array is not symmetric."
        }
    }
    return "Array is symmetric."
}

console.log(isArraySymmetric(["m", "a", "y", "a", "m"]));

// 5. Write a program that calculates the sum of positive elements in the array.
// Input array: [3, 11, -5, -3, 2]
// Output: 16

function sumPositiveElems(arr) {
    var output = 0;

    for (var i = 0; i < arr.length; i++) {
        if (arr[i] > 0) {
            output += arr[i];
        }
    }
    return output;
}

console.log(sumPositiveElems([3, 11, -5, -3, 2]));

// 4. Write a program that finds the second largest number and prints out its value.
// Input array: [3, 4, 2, 2, -1]
// Output: 2

function findSecondMax(arr) {
    var maximum = arr[0];
    // var secondMax = 0;  
    var max2;

    for (var i = 1; i < arr.length; i++) {
        if (maximum < arr[i]) {
            max2 = maximum;
            maximum = arr[i];
        }
        if (maximum > arr[i]) {
            if (max2 < arr[i]) {
                max2 = arr[i];
            }
        }
    }
    console.log(max2);

    return maximum;
}

console.log(findSecondMax([3, 4, 2, 2, -1]));

// 3. Write a program that finds the minimum of a given array and prints out its value and
// index.
// Input array: [4, 2, 2, -1, 6]
// Output: -1, 3


function findMinimum(arr) {
    var minimum = arr[0];
    var minIndex = 0;

    for (var i = 0; i < arr.length; i++) {
        if (arr[i] < minimum) {
            minimum = arr[i];
            minIndex = i;
        }
    }
    return minimum + " is @ place: " + minIndex;
}

console.log(findMinimum([4, 2, 2, -1, 6]));

// 1. Write a program that checks if a given element e is in the array a.
// Input: e = 3, a = [5, -4.2, 3, 7]
// Output: yes
// Input: e = 3, a = [5, -4.2, 18, 7]
// Output: no


function isElemInArray(arr, elem) {
    for (var i = 0; i < arr.length; i++) {
        if (arr[i] === elem) {
            return "Element is @ " + i + ". place."
        }
    }
    return "No."
}

console.log(isElemInArray([5, -4, 2, 3, 7], 3));