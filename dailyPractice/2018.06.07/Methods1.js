// map; -
// filter; -
// slice; -
// substring; -
// push; -
// pop; -
// indexOf; -
// lastIndexOf; -
// reverse; -
// sort; -
// includes;

//implement in ES2015

//1. Implement array.prototype.map().
// The map() method creates a new array with the results of calling a provided function
//  on every element in the calling array.



function mapp(arr, a) {
    const output = [];
    for (let i = 0; i < arr.length; i++) {
        output[i] = a(arr[i]);
    }
    return output;
}

function magnify(item) {
    return item * 2;
}

console.log(mapp([1, 2, 3], magnify));

// 2. filter
//The filter() method creates a new array with all elements 
// that pass the test implemented by the provided function.

function filterr(arr, b) {
    const output = [];
    let k = 0;
    for (let i = 0; i < arr.length; i++) {
        if (b(arr[i])) {
            output[k++] = arr[i];
        }
    }
    return output;
}

function isNumOdd(num) {
    if (num % 2 !== 0) {
        return true;
    }
    return false;
}

console.log(filterr([1, 2, 3, 4, 5], isNumOdd));

//3. slice-slice() extracts the text from one string and returns a new string. Changes to the text in one string do not affect the other string.
// slice() extracts up to but not including endIndex.
//str.slice(1, 4) extracts the second character through the fourth character (characters indexed 1, 2, and 3).
//arr.slice([begin[, end]])
//output: new array containing the extracted elements.
//str.slice(beginIndex[, endIndex])- FOR STRING
//if begin, end not spec.=> from 0 to last index;
//if begin>length=> "" or [];
//if begin or end <0=> length+begin/end

function sliced(arr, begin, end) {
    const output = [];

    for (let i = begin; i < end; i++) {
        output[i] = arr[i];
    }
    return output;
}

function sliceString(str, begin, end) {
    let output = "";
    for (let i = begin; i < str.length; i++) {
        output += str[i];
    }
    return output;
}

//4. substring() extracts characters from indexStart up to but not including indexEnd. In particular:
// If indexEnd is omitted, substring() extracts characters to the end of the string.
// If indexStart is equal to indexEnd, substring() returns an empty string.
// If indexStart is greater than indexEnd, then the effect of substring() is as
// if the two arguments were swapped;
// See example below.
// Any argument value that is less than 0 or greater than stringName.length is treated as
// if it were 0 and stringName.length respectively.Any argument value that is NaN is treated as
// if it were 0.

function substringy(str, start, stop) {
    let output = "";
    (start < 0) ? start = 0: start;
    if (stop < start) {
        let temp = start
        start = stop
        stop = temp;
    };
    (stop > str.length) ? stop = str.length: stop;

    for (let i = begin; i < stop; i++) {
        output += str[i];
    }
    return output;
}

//5.push & pop for ARRAYS;
//push returns new array length
//pop returns popped element
// -The pop() method removes the last element from an array and returns that element.
//  This method changes the length of the array.
//also see shift and unshift;

function pushy(arr, ...elements) {
    let output = 0;
    let i = arr.length;
    elements.forEach(element => {
        arr[i++] = element;
    })
    output = arr.length;
    return output;
}

console.log(pushy([1, 2, 3], 4, 5, 6));

function poppy(arr) {
    let output = arr[arr.length - 1];
    arr.length = arr.length - 1;
    return output;
}

console.log(poppy([1, 2, "a", 4, "m"]));

// 6.indexOf/lastIndexOf*
//The indexOf() method returns the first index at which a given element can be found in the array,
//  or -1 if it is not present.....arr.indexOf(searchElement[, fromIndex])
//For string :fromIndex values lower than 0 or greater than str.length,
//  the search starts at index 0 and str.length respectively.
//The lastIndexOf() method returns the last index at which a given element can be found in the array, or -1 if it is not present.
//  The array is searched backwards, starting at fromIndex.

function indexOfElem(arr, elem, begin) {
    for (let i = begin; i < arr.length; i++) {
        if (arr[i] === elem) {
            let output = i;
            return output;
        }
    }
    output = -1;
    return output;
}

console.log(indexOfElem([1, 2, 3, 3, 2, 3, 4, 5], 3, 1));

function lastIndexOfElemInArray(arr, elem, begin) {
    for (let i = begin; i > 0; i--) {
        if (arr[i] === elem) {
            let output = i;
            return output;
        }
    }
    output = -1;
    return output;
}

console.log(lastIndexOfElemInArray([1, 2, 2, 4, 5, 4, 2, 4], 2, 7));

//7. The reverse() method reverses an array in place.
//  The first array element becomes the last, and the last array element becomes the first.

function reverseArray(arr) {
    if (arr.length % 2 === 0) {
        var end = arr.length / 2;
    } else if (arr.length % 2 !== 0) {
        var end = (arr.length - (arr.length % 2)) / 2;
    }
    for (let i = 0; i < end; i++) {
        let temp = arr[i];
        arr[i] = arr[arr.length - i - 1];
        arr[arr.length - i - 1] = temp;
    }
    return arr;
}

console.log(reverseArray([1, 2, 3, 4, 5]));

//8. sort array.
// //arr.sort([compareFunction])
// Parameters: compareFunction Optional
// Specifies a function that defines the sort order. 
// If omitted, the array is sorted according to each character's Unicode code point value.
// Return value:The sorted array. Note that the array is sorted in place, and no copy is made.

function sortArray(arr, sorter) {
    for (let j = 0; j < arr.length; j++) {
        for (let i = 0; i < arr.length; i++) {
            if (sorter(arr[i], arr[i + 1]) > 0) {
                let temp = arr[i];
                arr[i] = arr[i + 1];
                arr[i + 1] = temp;
            }
        }
    }
    return arr;
}

console.log(sortArray([5, 4, 3, 2, 1], order));

function order(a, b) {
    if (a < b) {
        return -1;
    }
    if (a === b) {
        return 0;
    }
    if (a > b) {
        return 1;
    }
}

//9.includes-The includes() method determines whether an array includes a certain element,
//  returning true or false as appropriate.
//fromIndex Optional
// The position in this array at which to begin searching for searchElement.
//  A negative value searches from the index of array.length - fromIndex by asc. Defaults to 0.

function isElemIncludedInArray(arr, elem, from) {
    let output = false;
    (from < 0) ? from += arr.length: from;
    (!from) ? from = 0: from;
    for (let i = from; i < arr.length; i++) {
        if (arr[i] === elem) {
            output = true;
        }
    }
    return output;
}