//Searching for the Median
// The median of a single sorted array is trivial to find and is O(1) constant time.
// For example, a sorted array A = [5, 7, 9, 11, 15], which has an odd number of elements,
// has a unique median element 9 at index 2. Elements [5, 7] to the left are smaller than or equal to the median.
// Elements [11, 15] to the right are bigger than or equal to the median.
// The array has 5 elements with indexes in the range of 0 to 4, and the median element is at index (5-1)/2 = 2.
// In general, the median is at index (n-1)/2 if the number of elements in an array (n) is odd.
// For a sorted array with an even number of elements, two elements in the middle are medians.
// For example, A = [5, 7, 9, 11] has medians of 7 and 9 at indexes of 1 and 2. 
// In general, the medians are at index floor((n-1)/2) and at n/2. We could just pick the lower median,
// which leads to a single index computation no matter whether the array 
// has an odd or an even number of elements, to wit, floor((n-1)/2).
// Finding a median of two sorted arrays is more difficult and is no longer constant time.
// For example, two sorted arrays A = [5, 7, 9, 11, 15] and B = [1, 8] could be merged
// into a single sorted array in O(N) time to produce C = [1, 5, 7, 8, 9, 11, 15].
// This resulting array has 7 elements, with a median of 8 at index floor((7-1)/2) = 3.
// It's possible to do better than O(N), by using a modified binary search leading to O(lgN) performance.

//18. There are two sorted arrays nums1 and nums2 of size m and n respectively.
// Find the median of the two sorted arrays.

function medianFinder(nums1, nums2) {
    var result = 0;

    var joint = nums1.slice();
    var k = nums1.length;

    for (var i = 0; i < nums2.length; i++) {
        joint[k++] = nums2[i];
    }
    joint = sortArray(joint);
    var medianIndex = 0;
    if (isArrayLengthOdd(joint)) {
        medianIndex = Math.floor(joint.length / 2);
        result = joint[medianIndex];
    } else {
        medianIndex = joint.length / 2;
        var medianIndex2 = medianIndex - 1;
        result = (joint[medianIndex] + joint[medianIndex2]) / 2;
    }
    return result;
}
console.log(medianFinder([1, 3], [2, 4]));



function sortArray(arr) {
    var result = arr.slice();
    for (var i = 0; i < result.length; i++) {
        for (var j = 0; j < result.length; j++) {
            if (result[i] < result[j]) {
                var temp = result[i];
                result[i] = result[j];
                result[j] = temp;
            }
        }
    }
    return result;
}
// console.log(sortArray([4, 3, 1, 5, 6, 2]));
function isArrayLengthOdd(arr) {
    if (arr.length % 2 !== 0) {
        return true;
    } else {
        return false;
    }
}


//19.Given a binary array, find the maximum number of consecutive 1s in this array.
// Input: [1,1,0,1,1,1]
// Output: 3
// Explanation: The first two digits or the last three digits are consecutive 1s.
//     The maximum number of consecutive 1s is 3.

function findMaxConsecutiveOnes(nums) {
    var counter = 0;
    var occ = [];
    var j = 0;

    for (var i = 0; i < nums.length; i++) {
        if (nums[i] === 1) {
            counter += 1;
        } else {
            occ[j++] = counter;
            counter = 0;
        }
    }

    var maxOcc = occ[0];

    for (var k = 1; k < occ.length; k++) {
        if (maxOcc < occ[k]) {
            maxOcc = occ[k];
        }
    }
    if (counter < maxOcc) {
        return maxOcc;
    } else {
        return counter;
    }
}

console.log(findMaxConsecutiveOnes([1, 1, 0, 1, 1, 1]));

//20.moveZeroes while keeping relative order of non-zeros;
//input:[1,0,0,3,12]=>output:[1,3,12,0,0]

function moveZeroes(arr) {
    for (var i = arr.length - 2; i > -1; i--) {
        if (arr[i] === 0) {
            for (var j = i; j < arr.length - 1; j++) {
                var temp = arr[j];
                arr[j] = arr[j + 1];
                arr[j + 1] = temp;
            }
        }
    }
    return arr;
}

console.log(moveZeroes([4, 2, 4, 0, 0, 3, 0, 5, 1, 0]));

//21.How to check if all elements of an array are positive

function areArrayItemsPositive(arr) {
    var result = true;

    for (var i = 0; i < arr.length; i++) {
        if (arr[i] < 0) {
            result = false;
        }
    }
    return result;
}
console.log(areArrayItemsPositive([1, 2, 3, 4, 0, 4]));

// 22.linear vs binary search:
//
// A linear search looks down a list, one item at a time, without jumping. 
// In complexity terms this is an O(n) search -
// the time taken to search the list gets bigger at the same rate as the list does.

// A binary search is when you start with the middle of a sorted list,
// and see whether that's greater than or less than the value you're looking for,
// which determines whether the value is in the first or second half of the list.
// Jump to the half way through the sublist, and compare again etc.
// This is pretty much how humans typically look up a word in a dictionary
// (although we use better heuristics, obviously - if you're looking for "cat" you don't start off at "M").
// In complexity terms this is an O(log n) search - the number of search operations grows 
// more slowly than the list does, because you're halving the "search space" with each operation.

// As an example, suppose you were looking for U
// in an A-Z list of letters (index 0-25; we're looking for the value at index 20).
// 
// A linear search would ask:

// list[0] == 'U'? No.
// list[1] == 'U'? No.
// list[2] == 'U'? No.
// list[3] == 'U'? No.
// list[4] == 'U'? No.
// list[5] == 'U'? No.
// ...  list[20] == 'U'? Yes. Finished.

// The binary search would ask:

// Compare list[12] ('M') with 'U': Smaller, look further on. (Range=13-25)
// Compare list[19] ('T') with 'U': Smaller, look further on. (Range=20-25)
// Compare list[22] ('W') with 'U': Bigger, look earlier. (Range=20-21)
// Compare list[20] ('U') with 'U': Found it! Finished.
// Comparing the two:

// Binary search requires the input data to be sorted; linear search doesn't
// Binary search requires an ordering comparison; linear search only requires equality comparisons
// Binary search has complexity O(log n); linear search has complexity O(n) as discussed earlier
// Binary search requires random access to the data; linear search only requires sequential access
// (this can be very important - it means a linear search can stream data of arbitrary size)