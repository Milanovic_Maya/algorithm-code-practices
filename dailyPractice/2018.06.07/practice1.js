//find longest word in string;
//output:string containing longest word;

//function to turn string to array;

function split(str, spliter) {
    var output = [""];
    var k = 0;
    var countWords = 1;
    var m = 1;
    for (var j = 0; j < str.length; j++) {
        if (str[j] === " ") {
            countWords++;
            output[m++] = "";
        }
    }
    for (var i = 0; i < str.length; i++) {
        if (str[i] === spliter) {
            k++;
            ++i;
        }
        output[k] += str[i];
    }
    return output;
}

// console.log(split("Laki i Paki", " "));


function findLongestWord(str) {
    var arr = split(str, " ");
    var max = arr[0].length;
    var output = arr[0];

    for (var i = 1; i < arr.length; i++) {
        var l1 = arr[i].length;
        if (max < l1) {
            max = l1;
            output = arr[i];
        }
    }
    return output;
}

// console.log(findLongestWord("Laki i Patrivoje"));

//reverse a string

function reverseString(str) {
    var output = "";
    for (var i = (str.length - 1); i >= 0; i--) {
        output += str[i];
    }
    return output;
}

console.log(reverseString("LakiiPaki"));

//Using the JavaScript language, have the function SimpleAdding(num) add up all the numbers from 1 to num.
//  For example: if the input is 4 then your program should return 10 because 1 + 2 + 3 + 4 = 10. 
// For the test cases, the parameter num will be any number from 1 to 1000. 
//Input:12
// Output:78
// Input:140
// Output:9870

function SimpleAdding(num) {
    var output = 0;
    for (var i = 1; i <= num; i++) {
        output += i;
    }
    return output;
}

console.log(SimpleAdding(140));

//Using the JavaScript language, have the function LetterCapitalize(str)
// take the str parameter being passed and capitalize the first letter of each word. 
//Words will be separated by only one space.

function LetterCapitalize(str) {
    var output = "";
    var arr = str.split(" ");
    for (var i = 0; i < arr.length; i++) {
        arr[i] = arr[i][0].toUpperCase() + arr[i].slice(1);
    }
    return arr.join(" ");
}

console.log(LetterCapitalize("hello world"));

// Using the JavaScript language, have the function CheckNums(num1,num2) take both parameters
// being passed and return the string true if num2 is greater than num1,
// otherwise return the string false. If the parameter values are equal to each
// other then return the string -1. 

function CheckNums(num1, num2) {
    if (num1 < num2) {
        return "true";
    }
    if (num1 > num2) {
        return "false";
    }
    return "-1";
}

console.log(CheckNums(2, 8));


// Using the JavaScript language, have the function TimeConvert(num)
// take the num parameter being passed and return the number of hours
// and minutes the parameter converts to (ie. if num = 63 then the output should be 1:3).
// Separate the number of hours and minutes with a colon. 
// Sample Test Cases
// Input:126

// Output:"2:6"


// Input:45

// Output:"0:45"

function TimeConvert(num) {
    var minutes = num % 60;
    var hours = (num - minutes) / 60;
    if (num < 60) {
        minutes = num;
        hours = 0;
    }
    var output = hours + ":" + minutes;
    return output;
}

console.log(TimeConvert(126));