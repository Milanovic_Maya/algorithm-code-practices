// 12. Write a program that calculates the greatest common divisor of two integers. Note: The
// greatest common divisor of two non-zero integers is the greatest positive number that
// divides both numbers with no remainder.
// Input: 192 42 | 81 9
// Output: 6 | 9 //////////////Math needed! sorry!

// 11. Check if a given string is a palindrome.
// Input: eye | Geek | a nut for a jar of tuna
// Output: true | false | true

function ifGivenStringIsPalindrome(str) {
    var l = str.length;
    var end = 0;
    if (l % 2 !== 0) {
        end = (l - l % 2) / 2;
    } else {
        end = l / 2;
    }

    for (var i = 0; i < end; i++) {
        if (str[i] === str[l - i - 1]) {
            return true;
        }
    }
    return false;
}

console.log(ifGivenStringIsPalindrome("a nut for a jar of tuna"));

// 10. Write a program that checks if the entered number is a prime number (i.e. divisible only
//     by 1 and by itself).
//     Input: 17 | 15
//     Output: true | false

function isPrimeNumber(num) {
    if (num > 1) {

        if (num === 2 || num === 3 || num === 5) {
            return true;
        }
        if (num % 2 === 0 || num % 3 === 0 || num % 5 === 0) {
            return false;
        }
        return true;
    }
    return "MUst be >0."
}

console.log(isPrimeNumber(7));

// 9. Write a program that displays all the combinations of two numbers between 1 and 7.
// Don't display two of the same numbers at the same time. Display the number of possible
// combinations, as well. (E.g. (1.2),(2,1) is allowed, but not (1,1), (2,2)...).


function combineNums() {
    var output = [];
    var arr = [1, 2, 3, 4, 5, 6, 7];
    var k = 0;

    for (var i = 0; i < arr.length; i++) {
        for (var j = 0; j < arr.length; j++) {
            if (arr[i] !== arr[j]) {
                var elem1 = ("" + arr[i]) + "," + arr[j];
                var elem2 = ("" + arr[j]) + "," + arr[i];
                output[k] = elem1;
                output[k + 1] = elem2;
                k++;
            }
        }
    }
    return output;
}

console.log(combineNums());

// 8. Write a program that takes a string and prints its characters out in reversed order in the
// console.
// Input: Belgrade Institute of Technology
// Output: ygolonhceT fo etutitsnI edargleB

function reverseString(str) {
    var output = "";
    for (var i = str.length - 1; i >= 0; i--) {
        output += str[i];
    }
    return output;
}

console.log(reverseString("Miura Daichi ft Miyavi: Dancing with my fingers"));

// 7. Define a 10 element array. Take the first two letters from every string (that has at least 2
//     letters) in the array and create a new string from them. Print it out in the console.
//     Input: [ "M", "Anne", 12, "Steve", "Joe", "John", "David", "Mark", true, "A" ]
//     Output: AnStJoJoDaMa

function mixCharsFromStrings(arr) {
    var output = "";
    for (var i = 0; i < arr.length; i++) {
        if (typeof arr[i] === "string" && arr[i].length > 2) {
            var elem = arr[i][0] + arr[i][1];
            output += elem;
        }
    }
    return output;
}

console.log(mixCharsFromStrings(["M", "Anne", 12, "Steve", "Joe", "John", "David", "Mark", true, "A"]));

// 6. Write a program that uses a loop to add all the even numbers from 1 to 1000 and
// subtracts all the odd numbers 1 to 500 from the calculated sum. The result should then
// be multiplied by 12.5 and displayed in console.
// Output: 2350000


function mathTime() {
    var output = 0;
    var sum = 0;
    var minus = 0;

    for (var i = 1; i <= 1000; i++) {
        if (i % 2 === 0) {
            sum += i;
        }
        if (i % 2 !== 0) {
            if (i < 500) {
                minus += i;
            }
        }
    }
    output = (sum - minus) * 12.5;
    return output;
}

console.log(mathTime());

// 5. Sort a previously defined array in a descending order and display it in the console.
// Input: [ 13, 11, 15, 5, 6, 1, 8, 12 ]
// Output: [ 15, 13, 12, 11, 8, 6, 5, 1 ]


function sortArrayDescending(arr) {
    var output = [];
    for (var j = 0; j < arr.length; j++) {
        for (var i = 0; i < arr.length; i++) {
            if (arr[i] < arr[i + 1]) {
                var temp = arr[i];
                arr[i] = arr[i + 1];
                arr[i + 1] = temp;
            }
        }
    }
    return arr;
}

console.log(sortArrayDescending([13, 11, 15, 5, 6, 1, 8, 12]));


// 4. Sort a previously defined array. Place its sorted values into a new array whose values
// are equivalent to the first array's values multiplied by 2.
// Input: [ 13, 11, 15, 5, 6, 1, 8, 12 ]
// Output: [ 2, 10, 12, 16, 22, 24, 26, 30 ]

function sortArrayDoubled(arr) {
    var output = [];
    for (var j = 0; j < arr.length; j++) {
        for (var i = 0; i < arr.length; i++) {
            if (arr[i] > arr[i + 1]) {
                var temp = arr[i];
                arr[i] = arr[i + 1];
                arr[i + 1] = temp;
            }
        }
    }
    for (var k = 0; k < arr.length; k++) {
        output[k] = arr[k] * 2;
    }
    return output;
}

console.log(sortArrayDoubled([13, 11, 15, 5, 6, 1, 8, 12]));

// 3. Initialize two arrays. The first one should contain student names, the second one the
// number of points for each student. Display students' names with their corresponding
// grade. Use the following ranges:
// 51-60 -> 6,
// 61-70 -> 7,
// 71-80 -> 8,
// 81-90 -> 9,
// 91-100 -> 10.
// Input : [ "Micahel", "Anne", "Frank", "Joe", "John", "David", "Mark", "Bill" ], [ 50, 39, 63,
// 72, 99, 51, 83, 59 ]
// Output: Bill acquired 59 points and earned 6. Micahel acquired 50 points and failed to
// complete the exam.

function displayStudentGrades(studentList, pointList) {
    var message = "";
    var messageList = [];
    if (studentList.length === pointList.length) {
        var l = studentList.length;
        for (var i = 0; i < l; i++) {
            var grade = (pointList[i] - (pointList[i] % 10)) / 10;
            if (grade > 5) {
                message = studentList[i] + " acquired " + pointList[i] + " points and earned " + grade + ".";
            }
            if (grade <= 5) {
                message = studentList[i] + " acquired " + pointList[i] + " points and failed to complete the exam."
            }
            messageList[i] = message;
        }
        return messageList;
    }
    return "Number of grades must be same as number of students."
}

console.log(displayStudentGrades(["Micahel", "Anne", "Frank", "Joe", "John", "David", "Mark", "Bill"], [50, 39, 63,
    72, 99, 51, 83, 59
]));


// 2. Use the following array to make a new one by dividing its values by two and adding 5. If
// a given element's value is 0, change it to 20.
// Input: [ 3, 500, -10, 149, 53, 414, 1, 19 ]
// Output: [ 6.5, 255, 20, 79.5, 31.5, 212, 5.5, 14.5 ]


function changeArray(arr) {
    var output = [];
    var newItem;
    for (var i = 0; i < arr.length; i++) {
        if (arr[i] === 0) {
            arr[i] = 20;
        }
        newItem = (arr[i] / 2) + 5;
        output[i] = newItem;
    }
    return output;
}

console.log(changeArray([3, 500, -10, 149, 53, 414, 1, 19]));


// 1. Find the min and max element in the following array and switch their places. Print out the
// modified array in the console.
// Input: [ 3, 500, 12, 149, 53, 414, 1, 19 ]
// Output: [ 3, 1, 12, 149, 53, 414, 500, 19 ]


function switchMaxMin(arr) {
    var output = [];
    var max = arr[0];
    var min = arr[0];
    var maxIndex = 0;
    var minIndex = 0;

    for (var i = 0; i < arr.length; i++) {
        if (max < arr[i + 1]) {
            max = arr[i + 1];
            maxIndex = i + 1;

        }
        if (min > arr[i + 1]) {
            min = arr[i + 1];
            minIndex = i + 1;
        }
        output[i] = arr[i];
    }
    var temp = output[maxIndex];
    output[maxIndex] = output[minIndex];
    output[minIndex] = temp;
    return output;
}

console.log(switchMaxMin([3, 500, 12, 149, 53, 414, 1, 19]));