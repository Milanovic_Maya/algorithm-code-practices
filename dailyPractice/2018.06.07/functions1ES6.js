//7.number of digits of given number

function getNumberOfDigits(num) {
    let output = 0;

    while (num > 0) {
        num = (num - (num % 10)) / 10;
        output++;
    }
    return output;
}

console.log(getNumberOfDigits(5));

//8.calculate number of occurence of num in array

function numOfOcc(arr, num) {
    let output = 0;
    for (let i = 0; i < arr.length; i++) {
        if (arr[i] === num) {
            output++;
        }
    }
    return output;
}

console.log(numOfOcc([1, 2, 3, 4, 5, 1, 2, 3, 1, 2], 1));

//9.sum of odd element in array

function sumOdds(arr) {
    let output = 0;
    let filteredArr = arr.filter(item => {
        return item % 2 !== 0;
    });
    output = filteredArr.reduce((acc, item) => acc + item);
    return output;
}

console.log(sumOdds([1, 2, 3, 4, 5]));

//10. calculate num of occurence of letter in string; case insensitive;

function numOfOccStr(str, letter) {
    let output = 0;
    const strCopy = str.toLowerCase();
    const lettCopy = letter.toLowerCase();

    for (let i = 0; i < strCopy.length; i++) {
        if (strCopy[i] === lettCopy) {
            output++;
        }
    }
    return output;
}

console.log(numOfOccStr("MaximuM", "m"));

//11. concatenate string n times

function concateStr(str, n) {
    let output = str;

    for (let i = 1; i < n; i++) {
        output += str;
    }
    return output;
}

console.log(concateStr("Laki", 3));