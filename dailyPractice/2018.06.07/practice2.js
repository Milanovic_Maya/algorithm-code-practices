// Given an array which consists of non-negative integers and an integer m,
//  you can split the array into m non-empty continuous subarrays.
//  Write an algorithm to minimize the largest sum among these m subarrays.

// Input:
// nums = [7,2,5,10,8]
// m = 2

// Output:
// 18

// Explanation:
// There are four ways to split nums into two subarrays.
// The best way is to split it into [7,2,5] and [10,8],
// where the largest sum among the two subarrays is only 18.

//My example, m=2(hard-coded);

function compareSplits(arr) {
    var temp = [];
    var results = [];
    var maxis = [];

    for (var i = 0; i < arr.length; i++) {
        temp[i] = arr[i];
        var acc = 0;
        var a = temp.reduce((acc, elem) => {
            return acc + elem
        });

        var b = (arr.reduce((acc, elem) => {
            return acc + elem
        })) - a;

        var result = 0;
        result = b - a;

        results[i] = Math.abs(result);
        (a > b) ? maxis[i] = a: maxis[i] = b;
    }
    var min = results[0];
    var minIndex = 0;
    var output = 0;

    for (var j = 0; j < results.length; j++) {
        if (min > results[j]) {
            min = results[j];
            output = maxis[j];
        }
    }
    return output;
}

console.log(compareSplits([1, 2, 3, 4, 5]));