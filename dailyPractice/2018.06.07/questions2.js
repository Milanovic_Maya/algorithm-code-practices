// 10 common JavaScript interview questions


// 1How would you check if a number is an integer?

function isInteger(num) {
    if (num % 1 === 0) {
        return true;
    }
    return false;
}

console.log(isInteger(2));


// 2What will the following code output?
(function () {
    var a = b = 5;
})();

console.log(b); //5
// 3Write a function that would allow you to do this.
multiply(5)(6);

function multiply(num1) {
    return function (num2) {
        return num1 * num2;
    }
}
console.log(multiply(5)(6));

// 4When would you use the bind function?

// A good use of the bind function is when you have a particular 
// function that you want to call with a specific this value.
//  You can then use bind to pass a specific object to a function that uses a this reference.

// 5What does "use strict" do?
// The "use strict" literal is entered at the
//  top of a JavaScript program or at the top of a function and it helps
//  you write safer JavaScript code by throwing an error if a global variable is created by mistake.

// 6What is the difference between == and ===?

// 7How would you add your own method to the Array object so the following code would work?
// var arr = [1, 2, 3, 4, 5];
// var avg = arr.average();
// console.log(avg);

//through prototype:

function average(arr) {
    var sum = 0;
    for (var i = 0; i < arr.length; i++) {
        sum += arr[i];
    }
    return sum / arr.length;
}

console.log(average([1, 2, 3, 4, 5]));



// 8Explain what a callback function is and provide a simple example.

// A callback function is a function that is passed to
//  another function as an argument and is executed after some operation has been completed.



// 9What will the following code output?
console.log(0.1 + 0.2 === 0.3)

// This will surprisingly output false because of floating point errors in
//  internally representing certain numbers. 0.1 + 0.2 does not nicely come out to 0.3
//  but instead the result is actually 0.30000000000000004 because the computer cannot internally
//  represent the correct number. One solution
//  to get around this problem is to round the results when doing arithmetic with decimal numbers.



// 10How would you create a private variable in JavaScript?

// To create a private variable in JavaScript that cannot be changed you
//  need to create it as a local variable within a function.
//   Even if the function is executed the variable cannot be accessed outside of the function.
//    For example:

function func() {
    var priv = "secret code";
}

console.log(priv); // throws ReferenceError
// To access the variable, a helper function 
// would need to be created that returns the private variable.

function func() {
    var priv = "secret code";
    return function () {
        return priv;
    }
}

var getPriv = func();
console.log(getPriv()); // => secret code