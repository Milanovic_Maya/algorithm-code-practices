// 10.Write a program to find the most frequent item of an array.

function mostFreq(arr) {
    var result;
    var newArr = [];
    var occurence = [];
    var k = 0;

    for (var i = 0; i < arr.length; i++) {
        var exist = false;
        for (var j = 0; j < newArr.length; j++) {
            if (arr[i] === newArr[j]) {
                exist = true;
                occurence[j]++;
            }
        }
        if (!exist) {
            newArr[j] = arr[i];
            occurence[k] = 1;
            k++;
        }
    }
    result = newArr[0];
    for (var m = 0; m < occurence.length; m++) {
        if (occurence[0] < occurence[m + 1]) {
            result = newArr[m + 1]
        }
    }
    return result;
}

console.log(mostFreq([3, 'a', 'a', 'a', 2, 3, 'a', 3, 'a', 2, 4, 9, 3]));


// 9. Write a function to hide email address.
// "myemailaddress@bgit.rs" ->​ "myemail...@bgit.rs"

function hideEmail(str, n) {
    var output = "";
    (!n) ? n = 8: n;

    var indexOfMonkey = 0;
    var endString = "";
    var beginString = "";

    for (var i = 0; i < str.length; i++) {
        if (str[i] === "@") {;
            indexOfMonkey = i;
            for (var k = indexOfMonkey; k < str.length; k++) {
                endString += str[k];
            }
            break;
        }
        beginString += str[i];
    }

    var cutIndex = indexOfMonkey - n + 1;
    for (var j = 0; j < cutIndex; j++) {
        output += beginString[j];
    }
    output += "..." + endString;
    return output;
}

console.log(hideEmail("myemailaddress@bgit.rs", 6));

// 8. Write a function to find a word within a string.
// 'The quick brown fox', 'fox' ->​ "'fox' was found 1 times"
// 'aa, bb, cc, dd, aa', 'aa' ->​ "'aa' was found 2 times"


function ifStringIncludesWord(str, word) {
    var output = false;
    var counter = 0;

    for (var i = 0; i < str.length; i++) {
        for (var j = 0; j < word.length; j++) {
            if (str[i] === word[j]) {
                var compare = sliceString(str, i, word.length);
                if (compare === word) {
                    output = true;
                    counter++;
                }
            }
        }
    }
    return "Word is found " + counter + " times in sentence.";
}

function sliceString(str, begin, places) {
    var output = "";

    for (var i = begin; i < (begin + places); i++) {
        output += str[i];
    }
    return output;
};

console.log(ifStringIncludesWord('The quick fox brown fox runs fast as any fox in forest.', 'fox'));


// 7. Write a function that says whether a number is perfect.
// 28 ->​ 28 is a perfect number.
// Note: According to Wikipedia: In number theory, a perfect number is a positive integer that is equal to the sum
// of its proper positive divisors, that is, the sum of its positive divisors excluding the number itself (also known
// as its aliquot sum). Equivalently, a perfect number is a number that is half the sum of all of its positive divisors
// (including itself).
// E.g.: The first perfect number is 6, because 1, 2 and 3 are its proper positive divisors, and 1 + 2 + 3 = 6.
// Equivalently, the number 6 is equal to half the sum of all its positive divisors: (1 + 2 + 3 + 6) / 2 = 6. The next
// perfect number is 28 = 1 + 2 + 4 + 7 + 14. This is followed by the perfect numbers 496 and 8128.


function isNumberPerfect(num) {
    var output = false;
    var divisors = [];
    var resource = num;
    var temp1, temp2;


    while (resource > 1) {
        temp1 = resource % 2;
        temp2 = Math.floor(resource / 2);
        resource = temp1 + temp2;
        divisors.push(resource);
    }
    var sum = reduceArray(divisors);
    (sum === num) ? output = true: output;
    return output;
}

function reduceArray(arr) {
    var acc = 0;

    for (var i = 0; i < arr.length; i++) {
        acc += arr[i];
    }
    return acc;
}

console.log(isNumberPerfect(8128));

// 6. Write a function to create a specified number of elements with pre-filled numeric value
// array.
// 6, 0 ->​ [0, 0, 0, 0, 0, 0]
// 2, "none" ->​ ["none", "none"]
// 2 ->​ [null, null]

function createArray(num, item) {
    var result = [];

    for (var i = 0; i < num; i++) {
        result[i] = item;
    }
    return result;
}

console.log(createArray(2, "Meow"));


// 5. Write a function to get the last element of an array. Passing a parameter 'n' will return the
// last 'n' elements of the array.
// [7, 9, 0, -2] ->​ -2
// [7, 9, 0, -2], 2 ->​ [0, -2]

function sliceLastItemsFromArray(arr, numOfItems) {
    (!numOfItems) ? numOfItems = 1: numOfItems;
    var output = [];
    var begin = arr.length - numOfItems;
    var j = 0;
    for (var i = begin; i < arr.length; i++) {
        output[j++] = arr[i];
    }
    return output;
}

console.log(sliceLastItemsFromArray(["A", "B", "C", "Laki", "Paki"], 2));


// 4. Write a function that reverses a number. The result must be a number.
// 12345 ->​ 54321 // Output must be a number

function reverseNumber(num) {
    var output = 0;
    var digits = [];

    var proxyNumber = num;
    while (proxyNumber > 0) {
        var rest = proxyNumber % 10;
        digits.push(rest);
        var temp = proxyNumber - rest;
        proxyNumber = temp / 10;
    }
    var k = 0;

    for (var i = digits.length - 1; i >= 0; i--) {
        output += (digits[k++] * Math.pow(10, i));
    }

    return output;
}

console.log(reverseNumber(876));

// 3. Write a program to filter out ​falsy​ values from the array​.
// [NaN, 0, 15, false, -22, '', undefined, 47, null] ->​ [15, -22, 47]

function filterFalsy(arr) {
    var output = [];
    var k = 0;
    for (var i = 0; i < arr.length; i++) {
        if (!!arr[i]) {
            output[k++] = arr[i];
        }
    }
    return output;
}

console.log(filterFalsy([NaN, 0, 15, false, -22, '', undefined, 47, null]));

// 2. Write a program to join all elements of the array into a string skipping elements that are
// undefined, null, NaN or​ Infinity.
// [NaN, 0, 15, false, -22, '', undefined, 47, null]

function filterArray(arr) {
    var output = [];
    var j = 0;

    for (var i = 0; i < arr.length; i++) {
        if (arr[i] !== undefined && arr[i] !== null && (!isNaN(arr[i])) && isFinite(arr[i])) {
            output[j++] = arr[i];
        }
    }
    return output;
}

console.log(filterArray([NaN, 0, 15, false, -22, '', undefined, 47, null]));

// 1. Write a program to insert a string within a string at a particular position (default is 1,
//     beginning of a string).
//     "My random string", "JS " ->​ "JS My random string"
//     "My random string", "JS ", 10 ->​ "My random JS string"

function insertStringIntoSentence(sentence, str, position) {
    var output = "";
    (!position || position > (sentence.length - 1)) ? position = 0: position;

    for (var i = 0; i < sentence.length; i++) {

        if (i === position) {
            output += str;
        }
        output += sentence[i];
    }
    return output;
}

console.log(insertStringIntoSentence("I love my cats.", "two ", 10));