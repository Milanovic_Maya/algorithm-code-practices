function checkPalindrome(str) {
    let l = str.length;
    let i = 0;
    let result = false;

    if (l === 1) {
        result = true;
        return result;
    }
    let end = 0;
    if (l % 2 !== 0) {
        end = (l - l % 2) / 2;
    } else {
        end = l / 2;
    }
    for (i; i < end; i++) {
        if (str[i] === str[l - i - 1]) {
            result = true;;
        } else {
            result = false;
        }
    }
    return result;
}
// console.log(checkPalindrome("aaabbaaa"));


////////////////////////////////////////////

function makeArrayConsecutive2(statues, counter = 0) {
    //needed for counting missing statues;
    let arr = statues.slice().sort((a, b) => {
        return a - b;
    });

    if (arr[0] === undefined) {
        return counter;
    }
    // console.log(arr);


    if ((arr[1] - arr[0]) > 1) {
        if (arr[1] !== undefined) {
            counter += arr[1] - (arr[0] + 1);
        }
    }
    return makeArrayConsecutive2(arr.slice(1), counter)
}

// console.log(makeArrayConsecutive2([6, 2, 3, 8]));
////////////////////////////////////////////////////////////////////

function almostIncreasingSequence(sequence) {
    let counter = 0;
    for (let i = 1; i < sequence.length; i++) {
        if (sequence[i - 1] > sequence[i]) {
            counter++;
        }
    }

    if (counter > 1) {
        return false;
    } else {
        return true;
    }
}

// console.log(almostIncreasingSequence([1, 3, 2])); ///not working entirely!!!!!!!

//////////////////////////////////////////////////////////////


function matrixElementsSum(matrix) {
    let haunted = [];
    let output = 0;
    let ghostRoom = 0;

    for (let i = 0; i < matrix.length; i++) {
        for (let j = 0; j < matrix[i].length; j++) {
            if (matrix[i][j] === 0) {
                haunted.push(j); //push index of haunted one

            } else if (matrix[i][j] > 0) { //if not zero
                ghostRoom = 0;
                for (let k = 0; k < haunted.length; k++) { //check if is haunted index
                    //if j is haunted
                    if (j === haunted[k]) {
                        //check @ ghost room
                        ghostRoom++;
                    }
                }
                if (ghostRoom === 0) {
                    output += matrix[i][j];
                }
            }

        }
    }
    return output;
}
// console.log(matrixElementsSum([
//     [0, 1, 1, 2],
//     [0, 5, 0, 0],
//     [2, 0, 3, 3]
// ]));

// console.log(matrixElementsSum([
//     [1, 1, 1, 0],
//     [0, 5, 0, 1],
//     [2, 1, 3, 10]
// ])); //done!!!


/////////////////////////////////////////////////////////////////////////////////////

// maxLength

function allLongestStrings(arr) {
    let maxLength = arr[0].length;
    for (let i = 1; i < arr.length; i++) {
        if (maxLength < arr[i].length) {
            maxLength = arr[i].length;
        }
    }
    let result = arr.filter(item => {
        return item.length === maxLength;
    })
    return result;
}

console.log(allLongestStrings(["aba", "aa", "ad", "vcd", "aba"]));

/////////////////////////////////////////////////////////////////////


function commonCharacterCount(s1, s2) {
    let counter = 0;
    for (let i = 0; i < s1.length; i++) {
        for (let j = 0; j < s2.length; j++) {
            if (s1[i] === s2[j]) {
                counter++;
                s2 = replaceSTR(s2, j);
                break;
            }
        }
    }
    return counter;
}

function replaceSTR(str, index) {
    let output = [];

    let arr = str.split("");
    for (let i = 0; i < arr.length; i++) {
        if (i === index) {
            output[i] = "";
        } else {
            output[i] = arr[i];
        }
    }
    let result = output.join("");

    return result;
}
console.log(replaceSTR("adcaa", 4));


console.log(commonCharacterCount("aabcc", "adcaa"));