// 1. Write a function to check whether the `input` is a string or not.
// "My random string" -> true
// 12 -> false


function stringValidation(str) {
    if (typeof str === "string") {
        return true;
    }
    return false;
}

var result1 = stringValidation("laki");
var result1a = stringValidation(22);

console.log("result1 is ", result1, "result1a is ", result1a);

// 2. Write a function to check whether a string is blank or not.
// "My random string" -> false
// " " -> true
// 12 -> false
// false -> false

function isStringBlank(str) {
    if (str === " ") {
        return true;
    }
    return false;
}

console.log(isStringBlank(" "));

// 3. Write a function that concatenates a given string n times (default is 1).
// "Ha" -> "Ha"
// "Ha", 3 -> "HaHaHa"

function concatenateStringMultipleTimes(str, n) {
    (!n) ? n = 1: n;
    var result = "";
    for (var i = 0; i < n; i++) {
        result += str;
    }
    return result;
}

console.log(concatenateStringMultipleTimes("La", 4));


// 4. Write a function to count the number of letter occurrences in a string.
// "My random string", "n" -> 2


function countLetterOccurence(str, letter) {
    var counter = 0;

    for (var i = 0; i < str.length; i++) {
        if (str[i] === letter) {
            counter++;
        }
    }
    return counter;
}

console.log(countLetterOccurence("Laki i Paki", "i"));


// 5. Write a function to find the position of the ​first​ occurrence of a character in a string. The
// result should be in human numeration form. If there are no occurrences of the character,
// the function should return -1.


function findIndexOfFirstOccurence(str, char) {

    var result = -1;

    for (var i = 0; i < str.length; i++) {
        if (str[i] === char) {
            result = i + 1;
            return result;
        }
    }
    return result;
}
console.log(findIndexOfFirstOccurence("Division", "i"));


// 6. Write a function to find the position of the ​last​ occurrence of a character in a string. The
// result should be in human numeration form. If there are no occurrences of the character,
// function should return -1.

function findIndexOfLastOccurence(str, char) {

    var result = -1;

    for (var i = 0; i < str.length; i++) {
        if (str[i] === char) {
            result = i + 1;
        }
    }

    return result;
}

console.log(findIndexOfLastOccurence("Articulation", "t"));

// 7. Write a function to convert string into an array. Space in a string should be represented as
// “null” in new array.
// "My random string" -> ["M", "y", null, "r", "a"]
// "Random" -> ["R", "a", "n", "d", "o", "m"]

function convertStringIntoArray(str) {
    var result = [];

    for (var i = 0; i < str.length; i++) {
        result[i] = str[i];
        if (result[i] === " ") {
            result[i] = null;
        }
    }
    return result;
}

console.log(convertStringIntoArray("Laki i Paki"));


// 8. Write a function that accepts a number as a parameter and checks if the number is prime or
// not.
// Note:​ A prime number (or a prime) is a natural number greater than 1 that has no positive
// divisors other than 1 and itself.

function isNumberPrime(num) {
    if (num > 1) {
        if (num === 2 || num === 3 || num === 5) {
            return true;
        }
        if (num % 2 === 0 || num % 3 === 0 || num % 5 === 0) {
            return false;
        }
        return true;
    }
    return "Prime number must be greater than 1."
}

console.log(isNumberPrime(12));

// 9. Write a function that replaces spaces in a string with provided separator. If separator is not
// provided, use “-” (dash) as the default separator.
//  "My random string", "_" -> "My_random_string"
//  "My random string", "+" -> "My+random+string"
//  "My random string" -> "My-random-string"

function replaceStringSeparator(str, separator) {
    (separator === undefined) ? separator = "-": separator;
    var result = "";

    for (var i = 0; i < str.length; i++) {
        var char = str[i];
        if (char === " ") {
            result += separator;
        } else {
            result += char;
        }
    }
    return result;
}

console.log(replaceStringSeparator("Laki i Paki", " 0x0 "));

// 10. Write a function to get the first n characters and add “...” at the end of newly created string

function sliceString(str, n) {
    var result = "";
    for (var i = 0; i < n; i++) {
        result += str[i];
    }
    return result + "...";
}

console.log(sliceString("My secret is that I'm scared of spiders.", 12));

// 11. Write a function that converts an array of strings into an array of numbers. Filter out all
// non-numeric values.
// ["1", "21", undefined, "42", "1e+3", Infinity] -> [1, 21, 42, 1000]


function convertArrayOfStringIntoArrayOfNumbers(arr) {
    var result = [];
    var j = 0;

    for (var i = 0; i < arr.length; i++) {
        if (typeof parseFloat(arr[i]) === "number" && isFinite(arr[i])) {
            result[j++] = (parseFloat(arr[i]));
        }
    }
    return result;
}

console.log(convertArrayOfStringIntoArrayOfNumbers(["1", "21", undefined, "42", "1e+3", Infinity]));

// 12. Write a function to calculate how many years there are left until retirement based on the
// year of birth. Retirement for men is at age of 65 and for women at age of 60. If someone is
// already retired, a proper message should be displayed.

function untilRetirement(yob, gender) {
    var currentYear = new Date().getFullYear();
    (typeof parseInt(yob) !== "number") ? "You must enter numeric value" : yob;
    var result = currentYear - yob;
    var n;

    (gender === "female") ? n = 60: (gender === "male") ? n = 65 : n = 0;


    if (n) {
        if (result >= n) {
            return "You should be already retired."
        };
        if (result <= 18) {
            return "You are still young to work."
        };
        if (18 < result < n) {
            return "You have " + (n - result) + " years until retirement."
        };

    }
    return "Enter proper genre."
}

console.log(untilRetirement(1989, "female"));

// 13.Write a function to humanize a number (formats a number to a human-readable string) with
// the correct suffix such as 1st, 2nd, 3rd or 4th.
// 1 -> 1st
// 11 -> 11th
// Hint: num % 100 >= 11 && num % 100 <= 13

function ordinalNumbers(num) {

    var result = "";
    var remains = num % 100;

    if (remains > 10 && remains < 14) {
        result = num + "th";
    } else if (remains > 14) {
        var lastDigit = remains % 10;

        switch (lastDigit) {
            case 1:
                result = num + "st";
                break;
            case 2:
                result = num + "nd";
                break;
            case 3:
                result = num + "rd";
                break;
            default:
                result = num + "th";
                break;
        }
    }
    return result;
}


console.log(ordinalNumbers(3581));