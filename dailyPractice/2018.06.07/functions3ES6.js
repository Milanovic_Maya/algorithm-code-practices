// 1. Write a program to insert a string within a string at a particular position (default is 1,
//     beginning of a string).
//     "My random string", "JS " ->​ "JS My random string"
//     "My random string", "JS ", 10 ->​ "My random JS string"

function insertString(sentence, word, position) {
    (!position || position > (sentence.length - 1)) ? position = 0: position;
    let output = "";
    for (let i = 0; i < sentence.length; i++) {
        if (i === position) {
            output += word
        }
        output += sentence[i];
    }
    return output;
}

console.log(insertString("Laki & Paki", "My "));




//     2. Write a program to join all elements of the array into a string skipping elements that are
//     undefined, null, NaN or​ Infinity.
//     [NaN, 0, 15, false, -22, '', undefined, 47, null]

function arrayIntoSTr(arr) {
    let output = [];
    let j = 0;
    for (let i = 0; i < arr.length; i++) {
        if (arr[i]) {
            if (arr[i] !== undefined && arr[i] !== null && (!isNaN(arr[i])) && isFinite(arr[i])) {
                output[j++] = arr[i];
            }
        }
    }
    return output;
}

//     3. Write a program to filter out ​falsy​ values from the array​.
//     [NaN, 0, 15, false, -22, '', undefined, 47, null] ->​ [15, -22, 47]

console.log(arrayIntoSTr([NaN, 0, 15, false, -22, '', undefined, 47, null]));



//     4. Write a function that reverses a number. The result must be a number.
//     12345 ->​ 54321 // Output must be a number

function reverseDigits(num) {
    let output = "";

    while (num > 0) {
        let lastDigit = num % 10;
        output += lastDigit;
        num = (num - (num % 10)) / 10;
    }
    let result = 0;
    let k = 0;

    for (let i = output.length - 1; i >= 0; i--) {
        result += (output[k++] * Math.pow(10, i));
    }
    return result;
}

console.log(reverseDigits(12345));



//     5. Write a function to get the last element of an array. Passing a parameter 'n' will return the
//     last 'n' elements of the array.
//     [7, 9, 0, -2] ->​ -2
//     [7, 9, 0, -2], 2 ->​ [0, -2]


function pieceOfArray(arr, n) {
    (!n) ? n = 1: n;
    (n > arr.length) ? n = arr.length: n;
    let output = [];
    let j = 0;
    const begin = arr.length - n;
    for (let i = begin; i < arr.length; i++) {
        output[j++] = arr[i];
    }
    return output;
}

console.log(pieceOfArray([7, 9, 0, -2], 2));



//     6. Write a function to create a specified number of elements with pre-filled numeric value
//     array.
//     6, 0 ->​ [0, 0, 0, 0, 0, 0]
//     2, "none" ->​ ["none", "none"]
//     2 ->​ [null, null]


function createArray(n, item) {
    let output = [];
    (!item) ? item = null: item;
    for (let i = 0; i < n; i++) {
        output[i] = item;
    }
    return output;
}

console.log(createArray(4, "Laki"));



//     7. Write a function that says whether a number is perfect.
//     28 ->​ 28 is a perfect number.
//     Note: According to Wikipedia: In number theory, a perfect number is a positive integer that is equal to the sum
//     of its proper positive divisors, that is, the sum of its positive divisors excluding the number itself (also known
//     as its aliquot sum). Equivalently, a perfect number is a number that is half the sum of all of its positive divisors
//     (including itself).
//     E.g.: The first perfect number is 6, because 1, 2 and 3 are its proper positive divisors, and 1 + 2 + 3 = 6.
//     Equivalently, the number 6 is equal to half the sum of all its positive divisors: (1 + 2 + 3 + 6) / 2 = 6. The next
//     perfect number is 28 = 1 + 2 + 4 + 7 + 14. This is followed by the perfect numbers 496 and 8128.


function isNumPerfect(num) {
    let output = false;
    let divisors = [];
    let resource = num;
    let temp1, temp2;

    while (resource > 1) {
        temp1 = resource % 2;
        temp2 = Math.floor(resource / 2);
        resource = temp1 + temp2;
        divisors.push(resource);
    }
    let sum = divisors.reduce((acc, e) => acc + e);
    (sum === num) ? output = true: output;
    return output;
}

console.log(isNumPerfect(28));



//     8. Write a function to find a word within a string.
//     'The quick brown fox', 'fox' ->​ "'fox' was found 1 times"
//     'aa, bb, cc, dd, aa', 'aa' ->​ "'aa' was found 2 times"


function ifStringIncludesWord(str, word) {
    let counter = 0;

    for (let i = 0; i < str.length; i++) {
        for (let j = 0; j < word.length; j++) {
            if (str[i] === word[j]) {
                let compare = str.slice(i, word.length + i);
                if (compare === word) {
                    counter++;
                }
            }
        }
    }
    return counter;
}


console.log(ifStringIncludesWord('The quick fox brown fox runs fast as any fox in forest.', 'fox'));


//     9. Write a function to hide email address.
//     "myemailaddress@bgit.rs" ->​ "myemail...@bgit.rs"


function hideMail(str, n) {
    let output = "";

    let pt1 = "";
    let pt2 = "";
    let indexOfMonkey = 0;

    for (let i = 0; i < str.length; i++) {
        if (str[i] === "@") {
            indexOfMonkey = i;
            for (let j = indexOfMonkey; j < str.length; j++) {
                pt2 += str[j];
            }
            break;
        }
        pt1 += str[i];
    }

    let cutIndex = indexOfMonkey - n + 1;

    for (let k = 0; k < cutIndex; k++) {
        output += pt1[k];
    }
    output += "..." + pt2;
    return output;
}

console.log(hideMail("mayam893@gmail.com", 5));



//     10.Write a program to find the most frequent item of an array.
//     [3, 'a', 'a', 'a', 2, 3, 'a', 3, 'a', 2, 4, 9, 3]


function findOccurence(arr) {
    let result;
    let newArr = [];
    let occurence = [];
    let k = 0;
    let j = 0;
    let i = 0;

    for ( i; i < arr.length; i++) {
        let exist = false;
        for ( j; j < newArr.length; j++) {
            if (arr[i] === newArr[j]) {
                exist = true;
                occurence[j]++;
            }
        }
        if (!exist) {
            newArr[j] = arr[i];
            occurence[k] = 1;
            k++;
        }
    }
    result = newArr[0];
    for (let m = 0; m < occurence.length; m++) {
        if (occurence[0] < occurence[m + 1]) {
            result = newArr[m + 1]
        }
    }
    return result;
}

console.log(findOccurence([3, 'a', 'a', 'a', 2, 3, 'a', 3, 'a', 2, 4, 9, 3]));