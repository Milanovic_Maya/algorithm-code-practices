function moveZeros(a) {
    var cnt = 0;
    for (var i = 0; i < a.length; i++) {
        if (a[i] === 0) {
            cnt++;
            for (var j = i; j < a.length - 1; j++) {
                var temp = a[j];
                a[j] = a[j + 1];
                a[j + 1] = temp;
            }
            i = i - 1;
        }
        if (i === a.length - 1 - cnt) {
            return a;
        }
    }
    return a;
}

console.log(moveZeros([2, 0, 0, 1, 0, 0, 3, 6, 5, 0, 17, 0, 0]));

var moveZeroes = function (a) {

    for (var i = a.length - 2; i > -1; i--) {
        if (a[i] === 0) {
            for (var j = i; j < a.length - 1; j++) {
                var temp = a[j];
                a[j] = a[j + 1];
                a[j + 1] = temp;
            }
        }
    }
    return a;
};