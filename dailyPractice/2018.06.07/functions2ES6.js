//5.indexOf

function firstIndexOf(str, char) {
    let output = -1;
    for (let i = 0; i < str.length; i++) {
        if (str[i] === char) {
            output = i;
            return output;
        }
    }
    return output;
}

console.log(firstIndexOf("Fever", "e"));

//6.lastIndexOf

function lastIndexOf(str, char) {
    let output = -1;
    for (let i = str.length - 1; i > 0; i--) {
        if (str[i] === char) {
            output = i;
            return output;
        }
    }
    return output;
}

console.log(lastIndexOf("Zajecarsko", "a"));

//7.string=>array

function string2Array(str) {
    let output = [];
    let k = 0;
    for (let i = 0; i < str.length; i++) {
        if (str[i] === " ") {
            output[k++] = null;
        } else {
            output[k++] = str[i];
        }
    }
    return output;
}

console.log(string2Array("Laki i Paki"));

//7b.array=>string

function array2String(arr, sep) {
    let output = "";
    (!sep) ? sep = "": sep;
    for (let i = 0; i < arr.length; i++) {
        if (arr[i] === " ") {
            output += sep;
        } else {
            output += arr[i];
        }
    }
    return output;
}

console.log(array2String([1, 2, 3, " ", 5, 6], "-"));

//8. is number prime?

function isNumPrime(num) {
    let output = false;

    if (num === 2 || num === 3 || num === 5) {
        output = true;
    }
    if (num > 1) {
        if (num !== 2 || num !== 3 || num !== 5) {
            if (num % 2 !== 0 && num % 3 !== 0 && num % 5 !== 0) {
                output = true;
            }
        }
    }
    return output;
}

console.log(isNumPrime(71));

//9.replace space in string with custom char; def="-"

function replaceSpace(str, sep) {
    let output = "";
    (!sep) ? sep = "-": sep;
    for (let i = 0; i < str.length; i++) {
        if (str[i] === " ") {
            output += sep;
        } else {
            output += str[i];
        }
    }
    return output;
}

console.log(replaceSpace("Laki i Paki", "---"));

//10.get first n chars from string and add custom string; def="..."

function changeStr(str, n, add) {
    let output = "";
    (!add) ? add = "...": add;
    for (let i = 0; i < n; i++) {
        output += str[i];
    }
    return output + add;
}

console.log(changeStr("Don't let the beat stop", 9));

// 11.filter non numeric from array; 

function getAllNums(arr) {
    let output = [];
    let k = 0;
    for (let i = 0; i < arr.length; i++) {
        if (typeof (parseFloat(arr[i])) === "number") {
            if (isFinite(arr[i]) && !(isNaN(arr[i]))) {
                output[k++] = parseFloat(arr[i]);
            }
        }
    }
    return output;
}
console.log(getAllNums(["1", "21", undefined, "42", "1e+3", Infinity]));

// 12. Write a function to calculate how many years there are left until retirement based on the
// year of birth. Retirement for men is at age of 65 and for women at age of 60. If someone is
// already retired, a proper message should be displayed.


function untilRetirement(yob, gender) {
    let age = (new Date().getFullYear()) - parseInt(yob);

    let retirementLimit = 0;

    if (gender !== "female" && gender !== "male") {
        return "Invalid genre!"
    }

    (gender === "female") ? retirementLimit = 60: retirementLimit = 65;

    if (age >= retirementLimit) {
        return "You should be retired."
    }
    if (age <= 18) {
        return "Go to school first!"
    }

    let yearsTillRetirement = retirementLimit - age;
    return yearsTillRetirement;
}

console.log(untilRetirement(1989, "female"));

// 13.Write a function to humanize a number (formats a number to a human-readable string) with
// the correct suffix such as 1st, 2nd, 3rd or 4th.


function humanizeNumber(num) {
    let output = "";

    if (num % 100 < 14 && num % 100 > 10) {
        return output + num + "th";
    } else
    if (num % 10 === 1) {
        return output + num + "st";
    } else if (num % 10 === 2) {
        return output + num + "nd";
    } else if (num % 10 === 3) {
        return output + num + "rd";
    } else {
        return output + num + "th";
    }
}

console.log(humanizeNumber(113));