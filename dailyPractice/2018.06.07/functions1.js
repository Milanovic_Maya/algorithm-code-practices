function stars() {
    var result = "";
    for (var i = 0; i < 5; i++) {
        result += "";
        for (var j = 0; j < 5; j++) {
            result += "*\t";
        }
        result += "\t";
    }
    return result;
}

// console.log(stars());


//custom practice

//sort numbers in array: 1,3,2,4=> so that every number gets value of next biggest, if end =>-1;

//output: 3,4,4-1;

function sortNumbers(arr) {
    var output = [];
    var k = 0;

    for (var i = 0; i < arr.length; i++) {
        for (var j = i + 1; j < arr.length; j++) {
            if (arr[i] < arr[j]) {
                output[k++] = arr[j];
                break;
            }
        }
    }
    output.push(-1);
    return output;
}

console.log(sortNumbers([1, 3, 2, 4, 1, 5]));

//stars

function starry(n) {
    var output = "";
    for (var i = 0; i < n; i++) {
        for (var j = 0; j < n; j++) {
            if (i === 0 || i === (n - 1)) {
                output += " * ";
            } else {
                if (j === 0 || j === (n - 1)) {
                    output += " * "
                } else {
                    output += " \u2665 ";
                }
            }
        }
        output += "\n"
    }
    return output;
}

console.log(starry(5));

// 8. Write a function that calculates a number of appearances of a given number in a given
// array.


function numOfAppear(n, arr) {
    var output = 0;
    for (var i = 0; i < arr.length; i++) {
        if (arr[i] === n) {
            output++;
        }
    }
    return output;
}

console.log(numOfAppear(8, [1, 2, 3, 6, 44, 8, 8, 9, 4, 9, 8]));