// 15.Write a function that takes a list of strings and prints them, one per line, in a rectangular
// frame.:
// For example the list ["Hello", "World", "in", "a", "frame"] gets
// printed as:
// *********
// * Hello *
// * World *
// * in *
// * a *
// * frame *
// *********

function printFramedWords(str) {
    var output = "";
    var line = "";
    for (var j = 0; j < str.length + 4; j++) {
        line += "*";
    }
    for (var i = 0; i < str.length; i++) {
        output += "\n* " + str[i] + " *";
    }
    output = line + output + "\n" + line;
    return output;
}

console.log(printFramedWords(["Hello", "World", "in", "a", "frame"]));


// 14.The body mass index (BMI) is the ratio of the weight of a person (in kilograms) to the
// square of the height (in meters). Write a function that takes two parameters, weight and
// height, computes the BMI, and prints the corresponding BMI category:
// ● Starvation: less than 15
// ● Anorexic: less than 17.5
// ● Underweight: less than 18.5
// ● Ideal: greater than or equal to 18.5 but less than 25
// ● Overweight: greater than or equal to 25 but less than 30
// ● Obese: greater than or equal to 30 but less than 40
// ● Morbidly obese: greater than or equal to 40

function calculateBMI(weight, height) {
    var BMI = ((weight / (Math.pow((height / 10), 2))) * 100).toFixed(2);
    var category = "";

    switch (true) {
        case BMI < 15:
            category = "starvation";
            break;
        case BMI < 17.5:
            category = "anorexic";
            break;
        case BMI < 18.5:
            category = "underweigth";
            break;
        case BMI < 25 && BMI >= 18.5:
            category = "ideal";
            break;
        case BMI < 30 && BMI > 25:
            category = "overweight";
            break;
        case BMI < 40 && BMI >= 30:
            category = "obese";
            break;
        case BMI >= 40:
            category = "morbidly obese";
            break;
        default:
            "No category";
    }
    return category;
}

console.log(calculateBMI(63, 167));

// 13.Write a function to find all the numbers greater than the average.

function findGreater(...others) {
    let sum = others.reduce((acc, element) => {
        return acc + element;
    });

    return others.filter(element => {
        return element > (sum / others.length);
    })
}

console.log(findGreater(1, 2, 3, 4, 5));


function filterGreaterThanAverage() {

    var argList = arguments;
    var sum = 0;

    for (var i = 0; i < argList.length; i++) {
        sum += argList[i];
    }
    var average = sum / argList.length;

    var output = [];
    var k = 0;

    for (var j = 0; j < argList.length; j++) {
        if (argList[j] > average) {
            output[k++] = argList[j];
        }
    }
    return output;
}

console.log(filterGreaterThanAverage(1, 2, 3, 4, 5, 6, 7));

// 12.Write a function to find the average of N elements. Make the function flexible to receive
// dynamic number or parameters.

function findAverage() {
    var output = 0;
    var sum = 0;
    for (var i = 0; i < arguments.length; i++) {
        sum += arguments[i];
    }
    output = sum / arguments.length;
    return output.toFixed(2);
}

console.log(findAverage(1, 5, 3, 12, 22, 44, 89));

// 11.Write a function to find and return the first, middle and last element of an array if the array
// has odd number of elements. If number of elements is even, return just the first and the
// last. In other cases (empty array), input array should be returned.

function goArray(arr) {
    var n = arr.length;
    if (n % 2 === 0) {
        return [arr[0], arr[n - 1]];
    } else if (n % 2 !== 0) {
        var mid = ((n - (n % 2)) / 2);
        return [arr[0], arr[mid], arr[n - 1]];
    }
}

console.log(goArray([1, 2, 3, 4, 5, 6, 7, 8]));

// 10.Write a function to find the element that occurs most frequently.

function findMostAnnoyingElement(arr) {
    var result;
    var newArr = [];
    var occurence = [];
    var k = 0;

    for (var i = 0; i < arr.length; i++) {
        var exist = false;
        for (var j = 0; j < newArr.length; j++) {
            if (arr[i] === newArr[j]) {
                exist = true;
                occurence[j]++;
            }
        }
        if (!exist) {
            newArr[j] = arr[i];
            occurence[k] = 1;
            k++;
        }
    }
    result = newArr[0];
    for (var m = 0; m < occurence.length; m++) {
        if (occurence[0] < occurence[m + 1]) {
            result = newArr[n]
        }
    }
    return result;
}

console.log(findMostAnnoyingElement([1, 1, 1, 2, 1, 3, 3, 4]));

// 9. Write a function to find the median element of array. Median is middle element in sorted array.

function medianFinder(arr) {
    var output = 0;
    for (var j = 0; j < arr.length; j++) {
        for (var i = 0; i < arr.length; i++) {
            if (arr[i] < arr[i + 1]) {
                var temp = arr[i];
                arr[i] = arr[i + 1];
                arr[i + 1] = temp;
            }
        }
    }

    if (arr.length % 2 === 0) {
        output = arr[arr.length / 2];
    } else {
        output = arr[(arr.length - (arr.length % 2)) / 2]
    }

    return output;
}

console.log(medianFinder([1, 4, 3, 7, 11]));

// 8. Write a function to find the maximum and minimum elements. Function returns an array.

function miniMax(arr) {
    var max = arr[0];
    var min = arr[0];
    for (var i = 0; i < arr.length; i++) {
        if (max < arr[i + 1]) {
            max = arr[i + 1];
        }
        if (min > arr[i + 1]) {
            min = arr[i + 1];
        }
    }
    return [max, min];
}

console.log(miniMax([2, 4, 6, 8]));

// 7. Write a function to find the maximum element in array of numbers. Filter out all non-number
// elements.
function filterMaxi(arr) {
    for (var i = 0; i < arr.length; i++) {
        if (typeof arr[i] === "number") {
            var max = arr[i];
            if (max < arr[i + 1]) {
                max = arr[i + 1];
            }
        }
    }
    return max;
}

console.log(filterMaxi([undefined, "mi", 1, 3, 7, 9]));

// 6. Write a function to input temperature in Centigrade and convert to Fahrenheit.


function cToF(input) {
    return input * 9 / 5 + 32;
}

console.log(cToF(20));

// 5. Write a program that prints a multiplication table for numbers up to 12.

function multiplicationTable() {
    var output = "";
    for (var i = 1; i <= 12; i++) {
        for (var j = 1; j <= 12; j++) {
            output += i * j + " ";
        }
        output += "\n";
    }
    return output;
}

console.log(multiplicationTable());

// 4. Write a function that takes a number and returns array of its digits.

function numberToArray(n) {
    var output = [];
    if (typeof n === "number") {
        if (n) {
            var nToString = "" + n;
            for (var i = 0; i < nToString.length; i++) {
                output[i] = nToString[i];
            }
        }
    }
    return output;
}

console.log(numberToArray(2704989715));

// 3. Write a function that rotates a list by k elements.
// For example [1,2,3,4,5,6] rotated by two becomes [3,4,5,6,1,2]

function rotateList(k, arr) {
    var output = [];
    for (var i = 0; i < arr.length; i++) {
        var n = i - k;
        if (n < 0) {
            n = arr.length + n;
            output[n] = arr[i];
        } else if (n >= 0) {
            output[n] = arr[k + n];
        }
    }
    return output;
}

console.log(rotateList(4, [1, 2, 3, 4, 5, 6]));

// 2. Write a function that combines two arrays by alternatingly taking elements.
// [‘a’,’b’,’c’], [1,2,3] -> [‘a’,1,’b’,2,’c’,3]

function combineArrays(arr1, arr2) {
    var output = [];
    var k = 0;

    for (var i = 0; i < arr1.length; i++) {
        output[k + i] = arr1[i];
        output[k + i + 1] = arr2[i];
        k++;
    }
    return output;
}

console.log(combineArrays([1, 2, 3], ["a", "b", "c"]));


// 1. Write a function to count vowels in a provided string. If you are not aware of indexOf
// function, try to use switch statement.

function countVowels(str) {
    var output = 0;
    var vowels = ["a", "e", "i", "o", "u"];
    var counter = 0;

    for (var i = 0; i < str.length; i++) {
        for (var j = 0; j < vowels.length; j++) {
            if (str[i] === vowels[j]) {
                counter++;
            }
        }
    }
    return counter;
}

function countEveryVowel(str) {
    var vowels = ["a", "e", "i", "o", "u"];
    var result = "";

    var occ = [];
    for (var k = 0; k < vowels.length; k++) {
        occ[k] = 0;
    }

    for (var i = 0; i < str.length; i++) {
        for (var j = 0; j < vowels.length; j++) {
            if (str[i] === vowels[j]) {
                occ[j]++;
            }
        }
    }
    for (var m = 0; m < occ.length; m++) {
        result += "Vowel " + vowels[m] + " is used: " + occ[m] + " times.\n";
    }
    return result;
}


console.log(countVowels("Yiruma"));
console.log(countEveryVowel("Yiruma"));