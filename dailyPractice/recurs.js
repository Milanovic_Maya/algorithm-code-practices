function repeater(char, output = "", num = 1) {
    if (output.length === num) {
        return output;
    }
    const newOutput = output + char;

    return repeater(char, newOutput, num);
}

console.log(repeater('g', "", 6));

// Get the length of an array using recursion without accessing its length property.

// Input: {Array} array - array whose length is sought
// Output: {Number}function getLength(array,index=0) {
function getLength(array, index = 0) {
    if (array[index] === undefined) {
        return index;
    }
    const newIndex = index + 1;
    return getLength(array, newIndex)
}

// To check if you've completed the challenge, uncomment these console.logs!
console.log(getLength([1])); // -> 1
console.log(getLength([1, 2])); // -> 2
console.log(getLength([1, 2, 3, 4, 5])); // -> 5
// console.log(getLength([])); // -> 0

// Challenge: POW
// Write a function that takes two inputs, a base and an exponent,
//  and returns the expected value of base ^ exponent. For instance,
//   if our base is 2 and our exponent is 3, then return 8 because 2^3 = 2*2*2 = 8.

function pow(base, exponent) {
    if (exponent === 1) {
        return base;
    }
    return base * pow(base, exponent - 1)
}

// To check if you've completed the challenge, uncomment these console.logs!
console.log(pow(2, 4)); // -> 16
console.log(pow(3, 5)); // -> 243


//flow

function flow(input, arr /*, index = 0*/ ) {

    if (arr[0] === undefined) {
        return input;
    }
    const newInput = arr[0](input);

    return flow(newInput, arr.slice(1))
    // if (index === arr.length) {
    //     return input;
    // }
    // // if (index === arr.length - 1) {
    // //     return arr[arr.length - 1](input);
    // // }
    // const newInput = arr[index](input);
    // return flow(newInput, arr, index + 1)

}
//can be done without index-with slice(1) for arr-every flow call starts from first item in array

// To check if you've completed the challenge, uncomment this code!
function multiplyBy2(num) {
    return num * 2;
}

function add7(num) {
    return num + 7;
}

function modulo4(num) {
    return num % 4;
}

function subtract10(num) {
    return num - 10;
}
const arrayOfFunctions = [multiplyBy2, add7, modulo4, subtract10];
console.log(flow(2, arrayOfFunctions)); // -> -7