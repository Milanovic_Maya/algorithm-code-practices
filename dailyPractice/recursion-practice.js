// Reverse a string. The recursive function call should return the reversed result of the passed in string.
//  E.g.
// reverseStr("cowbell") --> "llebwoc"

function reverseString(str) {
    if (str === "") {
        return str;
    }
    return reverseString(str.substring(1)) + str[0];
}

console.log(reverseString("cat"));
//done



// Fibonacci number. Get the nth Fibonacci number as the return value. E.g.
// fibonacci(5) --> 5
// fibonacci(10) --> 55
// fibonacci(15) --> 610

function fibonacci(n) {
    if (n < 2 || n === 2) {
        return 1;
    }
    return fibonacci(n - 2) + fibonacci(n - 1);
}

console.log(fibonacci(15));

//done

//fibonacci print array till i;

function fib(i) {

    //base case:
    if (i === 1) {
        return [0, 1];
    }
    //recursion:
    var fibonacciArray = fib(i - 1);

    var l = fibonacciArray.length;

    //get next element:
    fibonacciArray.push(fibonacciArray[l - 1] + fibonacciArray[l - 2]);

    //return array of fibonacci nums;
    return fibonacciArray;
}

console.log(fib(5));



// Count the number of reoccurring instances of a digit in a number (E.g. 79092342 has two 9s).


//without recursion:

function countOccurencesOfNumbers(num) {
    var result = [];
    var numToString = num + "";

    var newArr = [];
    var k = 0;


    for (var i = 0; i < numToString.length; i++) {
        var exist = false;
        for (var j = 0; j < newArr.length; j++) {
            if (numToString[i] === newArr[j]) {
                exist = true;
                result[j]++;
            }
        }
        if (!exist) {
            newArr[j++] = numToString[i];
            result[k++] = 1;
        }
    }

    //using map as result holder:
    var output = new Map();

    for (var l = 0; l < result.length; l++) {
        var key = newArr[l];
        var value = result[l];
        output.set(key, value);
    }

    return output;
}

console.log(countOccurencesOfNumbers(2244005));

//real task:

//Think about it for a moment. If it is a negative number, the string will have a minimum length of 2. 
// If the number is greater than 9, will have a length of 2. 
// Therefore, only numbers from 0 to 9 will have a length of 1 when converted to a string. Pretty neat right?

// // Validation checks
// if (typeof digit !== "string") {
//     digit = digit.toString();
//     }
//     if (digit.length !== 1) {
//     throw new Error("Please enter a single digit.");
//     }

function getDigitOccurence(num, digit) {
    var output = 0;

    var str = num + "";
    var digitStr = digit + "";

    for (var i = 0; i < str.length; i++) {
        if (str[i] === digitStr) {
            output++;
        }
    }
    return output;
}

console.log(getDigitOccurence(2244005, 2));


//try with recursion:


function getDigitOccurenceWithRecursion(num, n, counter = 0) {
    var str = num + "";
    var digit = n + "";
    if (str.length === 0) {
        return counter;
    }

    if (str[0] === digit) {
        counter++;
    }
    return getDigitOccurenceWithRecursion(str.slice(1), digit, counter)
}

console.log(getDigitOccurenceWithRecursion(2244005, 0));




//  For bonus points, create a generator function using closures to create a recursive function using 
//  the value passed.
// E.g. Function can generate instances such as
// count7, count8
// which counts for the digits 7 and 8 respectively.



// Using recursion, go through a string and remove characters that occur more than once.
//  E.g. passing in "Troll" should return "trol". Passing in "abracadabra" should return "abrcd".
// Intermediate Exercises
// Okay, you are starting to get the hang of recursion. The following exercises should leave you
//  scratching your head slightly. No pain, no gain right?

// Find the greatest common divisor of two numbers.


// Find the lowest common multiple of two numbers. Assume that the two numbers are greater
//  than or equal to 2.




// Advanced Recursion Exercises
// The purpose of these exercises is to train your mind to be able to use recursion to solve
//  real world problems. When solving these problems, it often helps to hash out your thoughts vocally,
//   or by drawing activity diagrams.



// Write a binary search algorithm that accepts an array.
//  Readers can assume that the passed in array is sorted.


// Write your own implementation of the merge sort algorithm. 
// As mentioned in a previous section, the merge sort algorithm  divides the
//  a big task into smaller tasks. It is one of the better scaling algorithms with 
//  a worst case performance of n log (n). The purpose of this exercise is to train 
//  the reader to be able to break down a big task into smaller tasks. Therefore, 
//  I highly recommend readers to attempt this problem without looking at the answer beforehand.

// Try implementing a recursive data structure. A good example to start off would be
//  implementing the binary search tree from scratch. Note: This is quite a challenging exercise,
//   so take your time. Don’t beat yourself if you don’t get it on your first try.



// Use recursion to solve the following exercises.

// 1. Write a JavaScript program to calculate the factorial of a number. Go to the editor
// In mathematics, the factorial of a non-negative integer n, denoted by n!, is the product of all positive integers less than or equal to n. For example, 5! = 5 x 4 x 3 x 2 x 1 = 120 
// Click me to see the solution

// 2. Write a JavaScript program to find the greatest common divisor (gcd) of two positive numbers. Go to the editor
// Click me to see the solution.

// 3. Write a JavaScript program to get the integers in range (x, y). Go to the editor
// Example : range(2, 9)
// Expected Output : [3, 4, 5, 6, 7, 8]
// Click me to see the solution.

// 4. Write a JavaScript program to compute the sum of an array of integers. Go to the editor
// Example : var array = [1, 2, 3, 4, 5, 6]
// Expected Output : 21 
// Click me to see the solution.

// 5. Write a JavaScript program to compute the exponent of a number. Go to the editor
// Note : The exponent of a number says how many times the base number is used as a factor.
// 82 = 8 x 8 = 64. Here 8 is the base and 2 is the exponent.
// Click me to see the solution.

// 6. Write a JavaScript program to get the first n Fibonacci numbers. Go to the editor
// Note : The Fibonacci Sequence is the series of numbers: 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, . . . Each subsequent number is the sum of the previous two.
// Click me to see the solution.

// 7. Write a JavaScript program to check whether a number is even or not. Go to the editor
// Click me to see the solution.

// 8. Write a JavaScript program for binary search. Go to the editor
// Sample array : [0,1,2,3,4,5,6]
// console.log(l.br_search(5)) will return '5' 
// Click me to see the solution.

// 9. Write a merge sort program in JavaScript. Go to the editor
// Sample array : [34,7,23,32,5,62]
// Sample output : [5, 7, 23, 32, 34, 62]
// Click me to see the solution.