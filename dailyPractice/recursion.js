function callThyself(num) {
    if (num < 1) {
        return;
    }
    console.log(num);
    return callThyself(num - 1);
}
// console.log(callThyself(10));


function callThyName(num) {
    if (num < 1) {
        return;
    }
    callThyName(num - 1);
    console.log(num);
}

// console.log(callThyName(3));


function fib(n) {
    if (n < 2) {
        return n;
    }
    return fib(n - 1) + fib(n - 2);
}
// console.log(fib(10));


function printFib(i) {
    if (i === 1) {
        return i;
    }
    console.log(fib(i));
    i--;
    return printFib(i);
}

// console.log(printFib(10));

/////////////////////////////////////////////
var fibonacci_series = function (n) {
    if (n === 1) {
        return [0, 1];
    } else {
        var s = fibonacci_series(n - 1);
        s.push(s[s.length - 1] + s[s.length - 2]);
        return s;
    }
};

console.log(fibonacci_series(8));