// Generator function made from applying concepts in lecture
var randomNumberProcessor = function randomNumberProcessor(algorithm) {
    return function getRandomNumber(min, max) {
        return algorithm(min, max);
    };
};
// Compose randomNumberProcessor and generateRandomInt. Generate a random number (no decimal place).
var generateRandomInt = randomNumberProcessor(function (min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
});
var generateRandomDouble = randomNumberProcessor(function (min, max) {
    return Math.random() * (max - min) + min;
});

////***********************CLOSURES LECTURE FROM CODESMITH*********/////////////////////////
//////////////////////////////////////////////////////////////////////////////

function createFunction() {
    function log() {
        return "hello world"
    }
    return log;
}

// Uncomment these to check your work!
var myFunction = createFunction();
console.log(myFunction()); //should log: 'hello world'



//Create a function createFunctionWithInput that accepts one input and returns a function.
//  When that created function is called, it should return the input that was passed to
//  createFunctionWithInput when it was created.

// ADD CODE HERE

function createFunctionWithInput(input) {
    function logInput() {
        return input;
    }
    return logInput;
}

// UNCOMMENT THESE TO TEST YOUR WORK!
const sampleFunc = createFunctionWithInput('sample');
console.log(sampleFunc()); // should log: 'sample'
const helloFunc = createFunctionWithInput('hello');
console.log(helloFunc()); // should log: 'hello'


////////////////////////////////////////////////////////////


// Challenge: Scoping


// Examine the code for the outer function. Notice that we are returning a function
//  and that function is using variables that are outside of its scope. 
//  Uncomment those lines of code. Try to deduce the output before executing.

function outer() {
    var counter = 0; // this variable is outside incrementCounter's scope
    function incrementCounter() {
        counter++;
        console.log('counter', counter);
    }
    return incrementCounter;
}

var willCounter = outer();
var jasCounter = outer();

// Uncomment each of these lines one by one.
// Before your do, guess what will be logged from each function call.

willCounter();
willCounter();
willCounter();

// jasCounter();
// willCounter();

////////////////////////////////////////////////////////////////////////////////////

// Now we are going to create a function addByX that returns a function that will add an input by x.


// ADD CODE HERE
function addByX(num) {
    if (typeof num !== "number") {
        return "Input must be a number!"
    }

    function add(x) {
        return num + x
    }
    return add;
}


const addByTwo = addByX(2);

addByTwo(1); //should return 3
addByTwo(2); //should return 4
addByTwo(3); //should return 5

const addByThree = addByX(3);
addByThree(1); //should return 4
addByThree(2); //should return 5

const addByFour = addByX(4);
addByFour(4); //should return 8
addByFour(10); //should return 14

console.log(addByThree(10));

///////////////////////////////////////////////////////////////////////////////////

// Write a function once that accepts a callback as input and returns a function.
//  When the returned function is called the first time, it should call the callback and return that output.
//   If it is called any additional times, instead of calling the callback again it will simply return
//    the output value from the first time it was called.

function addTwo(num) {
    return num + 2;
}

//this one adds two all the time
// function once(callback) {
//     return function (num) {
//         var output = (function () {
//             return callback(num)
//         }());
//         return output;
//     }
// }

function once(callback) {

    var hasBeenCalled = false; // tells us if callback has been invoked
    var cashedResult; // stores result of callback;

    function innerFunc(...num) {
        if (!hasBeenCalled) { //if callback hasn't been called
            cashedResult = callback(...num); //invoke callback with arguments with operator rest;
            hasBeenCalled = true; //indicates that callback has been called
        }
        return cashedResult;
    }

    return innerFunc; //keeps secret link with once function, so when once returns, innerFunc can access its variables(hasBeenCalled)!!!!
}


const addByTwoOnce = once(addTwo); // function callParam;

console.log(addByTwoOnce(5)); //called first time : should log 7

console.log(addByTwoOnce(10)); // called any other time : should log 7;output from first call;


// UNCOMMENT THESE TO TEST YOUR WORK!
console.log(addByTwoOnce(9001)); //should log 7



// ADD CODE HERE

// Write a function after that takes the number of times the callback
// needs to be called before being executed as the first parameter
// and the callback as the second parameter.

function after(num, cb) {
    var timesCalled = 1;

    function innerFunc(str) {
        if (timesCalled === num) {

            return cb(str);
        }

        timesCalled += 1;
    }

    return innerFunc;
}

const called = function (string) {
    return ('hello ' + string);
};
const afterCalled = after(3, called);

// UNCOMMENT THESE LINES TO TEST YOUR WORK
console.log(afterCalled('world')); // -> prints undefined
console.log(afterCalled('world')); // -> prints undefined
console.log(afterCalled('world')); // -> prints 'hello world'



//Write a function delay that accepts two arguments,
// a callback and the wait time in milliseconds.
// Delay should return a function that, when
// invoked waits for the specified amount of time before executing.
// HINT - research setTimeout();


// ADD CODE HERE

function delay(cb, time) {

    function innerFunc() {
        return setTimeout(cb, time);
    }

    return innerFunc;
}


// UNCOMMENT THE CODE BELOW TO TEST DELAY
let count = 0;
const delayedFunc = delay(() => count++, 1000);
// delayedFunc();
// console.log(count); // should print '0'
// setTimeout(() => console.log(count), 1000); // should print '1' after 1 second


// Create a function saveOutput that accepts a function (that will accept one argument),
// and a string (that will act as a password). saveOutput will then return a function
// that behaves exactly like the passed-in function, except for when the password string
// is passed in as an argument. When this happens, the returned function will return an 
// object with all previously passed-in arguments as keys, and the corresponding outputs as values


// ADD CODE HERE

function saveOutput(cb, str) {
    //cb accepts one arg

    let passedArguments = {};

    function innerFunc(val) {
        let argument = val;

        if (argument !== str) {
            //stores result of cb in object
            passedArguments[argument] = cb(argument);

            //behaves like cb if argument !== str
            return cb(argument);

        } else if (argument === str) {
            //when passed str returns object with previous arguments
            return passedArguments;
        }
    }

    return innerFunc;
}


// Uncomment these to check your work!
const multiplyBy2 = function (num) {
    return num * 2;
};
const multBy2AndLog = saveOutput(multiplyBy2, 'boo');
console.log(multBy2AndLog(2)); // should log: 4
console.log(multBy2AndLog(9)); // should log: 18
console.log(multBy2AndLog('boo')); // should log: { 2: 4, 9: 18 }

/////////////////////////////////////////////////////////////////////////////////


// Create a function cycleIterator that accepts an array, and returns a function.
// The returned function will accept zero arguments. When first invoked,
// the returned function will return the first element of the array.
// When invoked a second time, the returned function will return the second element
// of the array, and so forth. After returning the last element of the array,
// the next invocation will return the first element of the array again, and
// continue on with the second after that, and so forth.

function cycleIterator(arr) {
    //count num of invocation
    var invocationCounter = 0;
    var temp;

    function innerFunc() {
        //if first invoked return first elem of arr
        //if reaches the end of arr, set invocationCounter to 0
        if (invocationCounter === arr.length - 1) {
            temp = invocationCounter;

            invocationCounter = 0;
            return arr[temp];
        }
        //if not edge element, just iterate farther;
        temp = invocationCounter;
        invocationCounter++;
        return arr[temp];
    }

    return innerFunc;
}

// ADD CODE HERE

// Uncomment these to check your work!
const threeDayWeekend = ['Fri', 'Sat', 'Sun'];
const getDay = cycleIterator(threeDayWeekend);
console.log(getDay()); // should log: 'Fri'
console.log(getDay()); // should log: 'Sat'
console.log(getDay()); // should log: 'Sun'
console.log(getDay()); // should log: 'Fri'
console.log(getDay()); // should log: 'Sat'
console.log(getDay()); // should log: 'Sun'


///////////////////////////////////////////////////////////////////////////////////////////////////////


// Create a function defineFirstArg that accepts a function and an argument.
// Also, the function being passed in will accept at least one argument.
// defineFirstArg will return a new function that invokes the passed-in
// function with the passed-in argument as the passed-in function's first argument.
// Additional arguments needed by the passed-in function will need to be passed into the returned function.

function defineFirstArg(cb, arg) {
    //cb accepts at least one argument-arg;

    function innerFunc(...additionalArgs) {
        //invokes cb(arg) with passed-in argument-first one;
        return cb(arg, ...additionalArgs);
        //additional arguments need to be passed to returned function;
        //but we need to unpack them with rest operator!!!
    }
    return innerFunc;
}


// ADD CODE HERE

// Uncomment these to check your work!
const subtract = function (big, small) {
    return big - small;
};
const subFrom20 = defineFirstArg(subtract, 20);
console.log(subFrom20(5)); // should log: 15

//////////////////////////////////////////////////////////////////////////////////////////


// Create a function dateStamp that accepts a function and returns a function.
// The returned function will accept whatever arguments the passed-in function
// accepts and return an object with a date key whose value is today's date
// (not including the time) represented as a human-readable string (see the Date object for conversion methods),
// and an output key that contains the result from invoking the passed-in function.

function dateStamp(cb) {

    function innerFunc(...args) {
        //innerFunc accepts arguments from cb

        let date = new Date().toDateString();
        let output = cb(...args);
        return {
            date,
            output
        }

        //returns object with date key; value:date{string} and output:result from invoking cb
    }
    return innerFunc;
}


// ADD CODE HERE

// Uncomment these to check your work!
const stampedMultBy2 = dateStamp(n => n * 2);
console.log(stampedMultBy2(4)); // should log: { date: (today's date), output: 8 }
console.log(stampedMultBy2(6)); // should log: { date: (today's date), output: 12 }


//////////////////////////////////////////////////////////////////////////////////////////////////////////


// Create a function censor that accepts no arguments. censor will return a function that will accept either two strings,
// or one string. When two strings are given, the returned function will hold onto the two strings as a pair, for future use.
// When one string is given, the returned function will return the same string, except all instances of a first string
// (of a saved pair) will be replaced with the second string (of a saved pair).

function censor() {
    let str1 = [];
    let str2 = [];
    let i = 0;

    function innerFunc(...args) {
        //accepts one or two strings

        let len = args.length;

        if (len === 2) {
            //if two strings=>store for future use
            str1[i] = args[0];
            str2[i] = args[1];
            i++;
        }
        if (len === 1) {
            //if one string => replace all str1 with str2;
            let sentence = args[0];

            let arraySentence = sentence.split(" ");
            //split string into array to easy manipulate with elements

            for (let j = 0; j < arraySentence.length; j++) {
                for (let k = 0; k < str1.length; k++) {
                    //if first letter from sentence is equal first letter from saved words
                    if (arraySentence[j][0] === str1[k][0]) {
                        //slice part of sentence to be checked
                        let word = arraySentence[j].slice(0, str1[k].length)
                        //if that part is equal word from saved words
                        if (areTwoStringsEqual(word, str1[k])) {
                            //replace part of sentence with censored word
                            arraySentence[j] = arraySentence[j].replace(str1[k], str2[k]);
                        }
                    }
                }
            }
            //join back array to string sentence;
            let sentenceCensored = arraySentence.join(" ");

            return sentenceCensored;
        }
    }
    return innerFunc;
}

function areTwoStringsEqual(a, b) {
    if (a === b) {
        return true
    }
    return false;
}


// ADD CODE HERE

// Uncomment these to check your work!

const changeScene = censor();
changeScene('dogs', 'cats');
changeScene('quick', 'slow');
console.log(changeScene('The quick, brown fox jumps over the lazy dogs.'));
// should log: 'The slow, brown fox jumps over the lazy cats.'