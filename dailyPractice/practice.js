const fib = (n) => {
    if (n <= 2) {
        return 1;
    };
    return fib(n - 2) + fib(n - 1);
};

console.log(fib(8));

const rev = (str) => {
    if (str === "") {
        return str;
    }
    return rev(str.slice(1)) + str[0];
}
console.log(rev("Cicima"));

const filt = (arr) => {
    var result;
    var newArr = [];
    var occurence = [];
    var k = 0;

    for (var i = 0; i < arr.length; i++) {
        var exist = false;
        for (var j = 0; j < newArr.length; j++) {
            if (arr[i] === newArr[j]) {
                exist = true;
                occurence[j]++;
            }
        }
        if (!exist) {
            newArr[j] = arr[i];
            occurence[k] = 1;
            k++;
        }
    }
    result = newArr[0];
    for (var m = 0; m < occurence.length; m++) {
        if (occurence[0] < occurence[m + 1]) {
            result = newArr[m + 1]
        }
    }
    return result;
}

console.log(filt([1, "a", 2, "a", 1, "b", "b", 2]));