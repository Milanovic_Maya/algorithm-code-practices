// document.querySelectorAll() returns a NodeList, which is an array like object.
// Write a function that takes a CSS selector and returns an array of Nodes selected.


function getArrayOfNodes(selector) {
    var nodeList = document.querySelectorAll(selector);
    return Array.prototype.slice.call(nodeList);
}


// Create a function that accepts an array of key value pairs and sets the value to the item
// that this keyword is pointing at and return that object. If this is null or undefined,
// create a new object.
//  E.g. set.call( {name: "jay"}, {age: 10, email: 'test@gmail.com'});
// return {name: "jay", age: 10, email: 'test@gmail.com'}

//gets array of objects;

function set() {
    "use strict"
    if (this === undefined || this === null) {
        return Object.create({});
    }

    var params = Array.prototype.slice.call(arguments);

    for (var i = 0; i < params.length; i++) {
        Object.assign(this, params[i])
    }

    //returns object but with indexes 0,1 for every item from arr
    //so we get object with properties "0","1" and values objects from arr
    // Object.assign(this, arr)

    return this;
}

// console.log(obj.set([{
//     name: "Laki"
// }, {
//     age: "1"
// }]));



var obj2 = {};
// obj2.set = obj.set.bind(obj2, [{
//     name: "jay"
// }, {
//     age: 10,
//     email: 'test@gmail.com'
// }]);
// console.log(obj2.set());


// console.log(set.call({
//         name: "jay"
//     }, {
//         age: 10,
//         email: 'test@gmail.com'
//     }
//     /*, {
//         color: "yellow"
//     }*/
// ));

// console.log(set());


// Create a function that accepts an array of key value pairs and sets the value 
// to the item that this keyword is pointing at and return that object.
// If this is null or undefined, create a new object.
// E.g. set.apply( {name: "jay"}, [{age: 10}]); // return {name: "jay", age: 10}

function set2(...arr) {
    if (!this) {
        return Object.create({});
    }
    if (!arr) {
        return "Insert proper values."
    }

    for (var i = 0; i < arr.length; i++) {
        Object.assign(this, arr[i]);

    }
    return this;
}

// console.log(set2([{
//     name: "Paki"
// }]));

// console.log(set2.apply({
//     name: "jay"
// }, [{
//     age: 10
// }])); // return {name: "jay", age: 10});





// Create a function similar to Math.max and min, but one one that applies calculations. The first two arguments 
// should be numbers. Make sure to convert the arguments after the second into an array of functions.
//  A sample template to get started with is provided below


function operate() {
    if (arguments.length < 3) {
        throw new Error("Must have at least three arguments");
    }
    if (typeof arguments[0] !== 'number' || typeof arguments[1] !== 'number') {
        throw new Error("first two arguments supplied must be a number");
    }
    // Write code ...
    // An array of functions. Hint use either call, apply or bind.
    //  Don't iterate over arguments and place functions in new array.

    var args;
    var result = 0;

    //array of functions;
    args = Array.prototype.slice.call(arguments, 2);

    //need to apply every function on first two arguments elements
    //and then sum that;
    //return that sum as result;

    for (var i = 0; i < args.length; i++) {
        result += args[i].call(null, arguments[0], arguments[1]);
    }

    return result;
    // Good luck
}

function sum(a, b) {
    return a + b;
}

function multiply(a, b) {
    return a * b;
}
console.log(operate(10, 2, sum, multiply)); // must return 32 -> (10 + 2) + (10 * 2) = 32