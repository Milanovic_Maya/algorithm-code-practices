function Art(item) {
    this.art = item.art;
    this.number = item.number;
}

Art.prototype.getArt = function () {
    return (this.art)[0].toUpperCase() + this.art.slice(1) + " is the " + this.number + "-th art.";
}

var workOfart = {
    art: "cinema",
    number: 7
};

var movie = new Art(workOfart);
// console.log(movie.getArt());

function Movie(movie) {
    Art.call(this, movie)
    this.title = movie.title;
    this.genre = movie.genre;
    this.year = movie.year;
}

Movie.prototype = Object.create(Art.prototype);
Movie.prototype.constructor = Movie;
//Object.create method is that you pass into it an object that you want to inherit from,
//  and it returns a new object that inherits from the object you passed into it.


//overriding:

Movie.prototype.getArt = function () {
    return this.title + " is " + this.genre + " " + this.art;
}

//calling a method from Movie's prototype object that is method inherited from parent constructor, here Art. 

Movie.prototype.getMessage = function () {
    var message = Object.getPrototypeOf(Movie.prototype).getArt.call(this);
    return "All should know that : " + message;
}

var ajin = {
    art: "cinema",
    number: 7,
    title: "Ajin-demi human",
    genre: "live-action",
    year: 2017
}

var jmovie1 = new Movie(ajin);
console.log(jmovie1.getMessage());