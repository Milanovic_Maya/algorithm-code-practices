function Meeting(day) {
    this.date = day;
    this.meetingDay = this.getDate();
}

Meeting.prototype.getDate = function () {
    return new Date (this.date).toLocaleDateString();
}

var meeting1 = new Meeting("2018/05/23");
console.log(meeting1);
