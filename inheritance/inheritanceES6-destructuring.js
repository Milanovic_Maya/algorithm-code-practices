class Weapon {
    constructor({
        sharp = false,
        material,
        name,
        originCountry
    }) {
        this.sharp = sharp;
        this.material = material;
        this.name = name;
        this.originCountry = originCountry;
    }
}

const blade = {
    sharp: true,
    material: "metal",
    name: "blade",
    originCountry: "Mesopotamia"
}

const bladeWeapon = new Weapon(blade);
// console.log(bladeWeapon);

class Sword extends Weapon {
    constructor(weapon) {
        super(weapon);
        this.model = weapon.model;
    }
}

const jsword = {
    sharp: true,
    material: "steel",
    name: "katana",
    originCountry: "Nippon",
    model: "odachi"
}

const katana = new Sword(jsword);

console.log(katana);