const cat = {
    name: "Laki",
    color: "Sand",
    age: 1,
    eyes: "amber",
    voice: "Meeow",
    breed: "kitty"
};
// console.log(cat);

const creature1 = {
    breed: "mouse",
    voice: "squeeek!"
}

class Animal {
    constructor(
        animal
    ) {
        this.breed = animal.breed;
        this.voice = animal.voice;
    }
    getBreed() {
        return `Hi, I'm little ${this.breed}. ${this.voice}!!!`;
    }
}
const mouse = new Animal(creature1);
// console.log(mouse);
// console.log(mouse.getBreed());

class Cat extends Animal {
    constructor(animal) {
        super(animal);
        this.name = animal.name;
        this.age = animal.age;
        this.color = animal.color;
        this.eyes = animal.eyes;
    }

    sayName() {
        return `Hello, I'm ${this.name} the ${this.breed}.`;
    }
}

const laka = new Cat(cat);
console.log(laka.getBreed());