function Music(song) {
    this.genre = song.genre;
    this.abbrev = this.getGenre();
}
Music.prototype.getGenre = function () {
    return (this.genre[0] + this.genre[this.genre.length - 1]).toUpperCase();
}

var real = {
    genre: "rock"
}

var isReal = new Music(real);
console.log(isReal);