var filterMyArray = function (arr) {
    var newArr = [];
    var k = 0;
    var occure = [];
    var finalArray = [];

    for (var i = 0; i < arr.length; i++) {
        var exist = false;
        for (var j = 0; j < newArr.length; j++) {
            if (arr[i] === newArr[j]) {
                exist = true;
                occure[j]++;
            }
        }
        if (!exist) {
            newArr[k] = arr[i];
            occure[k] = 1;
            k++;
        }
    }

    for (var m = 0; m < occure.length; m++) {
        if (occure[m] % 2 !== 0) {
            finalArray.push(newArr[m]);
        }
    }
    return finalArray;
}

var result = filterMyArray([1, 1, 2, 3, 3, 3]);

console.log(result);