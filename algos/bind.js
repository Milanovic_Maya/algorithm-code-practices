var x = {
    foo: function () {
        return this;
    }
}


var y = {};


var z = y;

var x1 = x.foo();
y.fun = x.foo.bind(y);

console.log(x.foo());
console.log(y);
console.log(z);
console.log(z);
console.log(x1 === x.foo());